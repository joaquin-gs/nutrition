<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient', function (Blueprint $table) {
            $table->string('patientID', 11)->primary();
            $table->string('referredBy', 50)->nullable();
            $table->float('initialWeight', 10, 0)->nullable();
            $table->string('diagnosis', 10)->nullable();
            $table->tinyInteger('motherAge')->nullable();
            $table->tinyInteger('numberOfChildren')->nullable();
            $table->string('remarks', 400)->nullable();
            $table->char('hasHIV', 1)->nullable();
            $table->tinyInteger('nutritionCase')->nullable();
            $table->dateTime('createdOn')->nullable();
            $table->char('createdBy', 5)->nullable();
            $table->char('activeFoodSupport', 1)->default('Y');
            $table->date('lastSeen')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
