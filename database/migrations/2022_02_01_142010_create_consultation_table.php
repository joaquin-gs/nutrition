<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultation', function (Blueprint $table) {
            $table->string('patientID', 11);
            $table->dateTime('visitDate');
            $table->integer('fromUnit')->nullable();
            $table->tinyInteger('height')->nullable();
            $table->float('weight', 10, 0)->nullable();
            $table->string('remarks', 400)->nullable();
            $table->tinyInteger('treatmentId');
            $table->dateTime('dischargedOn')->nullable();
            $table->char('dischargedBy', 5)->nullable();

            $table->primary(['patientID', 'visitDate']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultation');
    }
}
