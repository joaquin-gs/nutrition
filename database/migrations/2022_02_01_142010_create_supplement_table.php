<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplement', function (Blueprint $table) {
            $table->char('weekYear', 6);
            $table->string('dayName', 10);
            $table->string('patientID', 11);
            $table->date('expiry')->nullable();
            $table->string('sugar', 8)->nullable();
            $table->string('rice', 5)->nullable();
            $table->string('pork', 8)->nullable();
            $table->string('beef', 8)->nullable();
            $table->string('chicken', 8)->nullable();
            $table->string('fish', 8)->nullable();
            $table->char('egg', 2)->nullable();
            $table->string('cannedFish', 5)->nullable();
            $table->string('oil', 8)->nullable();
            $table->string('veggies', 45)->nullable();
            $table->string('hygiene', 45)->nullable();

            $table->primary(['weekYear', 'dayName', 'patientID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplement');
    }
}
