<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppliedTreatmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applied_treatment', function (Blueprint $table) {
            $table->increments('treatmentId');
            $table->string('patientID', 11);
            $table->dateTime('visitDate');
            $table->string('treatmentType', 50)->nullable();
            $table->string('subtype1', 40)->nullable();
            $table->string('subtype2', 40)->nullable();
            $table->tinyInteger('quantity')->nullable();
            $table->tinyInteger('frequency')->nullable();
            $table->tinyInteger('duration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applied_treatment');
    }
}
