<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->char('weekYear', 6);
            $table->string('dayName', 10);
            $table->string('meat', 20)->nullable();
            $table->string('veggies', 45)->nullable();
            $table->string('rice', 10)->nullable();
            $table->string('eggs', 10)->nullable();
            $table->string('oil', 10)->nullable();
            $table->string('sugar', 10)->nullable();
            $table->string('hygiene', 45)->nullable();

            $table->primary(['weekYear', 'dayName']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
