<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToServingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('serving', function (Blueprint $table) {
            $table->foreign(['weekYear', 'dayName'], 'FK_serving_menu')->references(['weekYear', 'dayName'])->on('menu')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['patientID'], 'FK_serving_patient')->references(['patientID'])->on('patient')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('serving', function (Blueprint $table) {
            $table->dropForeign('FK_serving_menu');
            $table->dropForeign('FK_serving_patient');
        });
    }
}
