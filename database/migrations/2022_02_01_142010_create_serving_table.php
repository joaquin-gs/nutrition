<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serving', function (Blueprint $table) {
            $table->char('weekYear', 6);
            $table->string('dayName', 10);
            $table->string('patientID', 11)->index('FK_serving_patient');
            $table->char('received', 1)->default('N');
            $table->string('officer', 50);
            $table->dateTime('createdOn');

            $table->primary(['weekYear', 'dayName', 'patientID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serving');
    }
}
