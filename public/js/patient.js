$(() => {
   $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
   });

   var datarow;
   var viewPortHeight = window.visualViewport.height;
   viewPortHeight = viewPortHeight * 80 / 100;

   //-----------------------------
   // jqxGrid settings
   //-----------------------------
   var source = {
      type: 'get',
      url: '/patient/getAllPatients',
      datatype: 'json',
      datafields: [
         { name: 'patientID', type: 'string' },
         { name: 'patientNameKh', type: 'string' },
         { name: 'patientNameEn', type: 'string' },
         { name: 'referredBy', type: 'string' },
         { name: 'initialWeight', type: 'number' },
         { name: 'diagnosis', type: 'string' },
         { name: 'motherAge', type: 'number' },
         { name: 'numberOfChildren', type: 'number' },
         { name: 'remarks', type: 'string' },
         { name: 'hasHIV', type: 'string' },
         { name: 'nutritionCase', type: 'number' },
         { name: 'date_birth', type: 'date' },
         { name: 'sex', type: 'string' },
         { name: 'province_name_khmer', type: 'string' },
         { name: 'district_name_khmer', type: 'string' },
         { name: 'commune_name_khmer', type: 'string' },
         { name: 'village_name_khmer', type: 'string' },
         { name: 'phone1', type: 'string' },
         { name: 'activeFoodSupport', type: 'string' },
         { name: 'createdOn', type: 'date' }
      ],
   };
   dataAdapter = new $.jqx.dataAdapter(source);

   $("#grid").jqxGrid({
      theme: 'darkblue',
      source: dataAdapter,
      width: '80%',
      height: viewPortHeight,
      altrows: true,
      sortable: true,
      filterable: true,
      showtoolbar: false,
      columnsresize: true,
      showfilterrow: true,
      showstatusbar: true,
      columns: [
         { text: 'Id', datafield: 'patientID', width: '10%' },
         { text: 'English name', datafield: 'patientNameEn', hidden: true },
         { text: 'Khmer name', datafield: 'patientNameKh', width: '25%', filterable: true },
         { text: 'Registered on', datafield: 'createdOn', width: '15%', cellsformat: 'dd-MM-yyyy HH:mm:ss', filterable: false },
         { text: 'Nutrition case', datafield: 'nutritionCase', width: '20%', filterable: false },
         { text: 'Initial weight', datafield: 'initialWeight', width: '10%', filterable: false },
         { text: 'Has HIV', datafield: 'hasHIV', width: '8%', filterable: false },
         { text: 'Food support', datafield: 'activeFoodSupport', width: '10%', filterable: true },
      ],
      // Adding maintenance buttons and their event handlers to the grid.
      renderstatusbar: function(statusbar) {
         var container = $("<div class='grid_buttons'></div>");
         statusbar.append(container);
         container.append('<button class="btn btn-sm btn-outline-light" id="newBtn">Register</button>');
         container.append('<button class="btn btn-sm btn-outline-light" id="editBtn">Edit</button>');
         container.append('<button class="btn btn-sm btn-outline-light" id="deleteBtn">Delete</button>');
         container.append('<button class="btn btn-sm btn-outline-light" id="closeGridBtn">Close</button>');

         $('#newBtn').on('click', function() {
            $('#patientsGrid').removeClass('active');
            $('#registerPatient').addClass('active');
         });

         $('#editBtn').on('click', function() {
            if (datarow != null) {
               // Show the window.
               // Populate the form.
               // Show the form.
            } else {
               toastr["warning"]('Please select a row to edit.', 'Contacts');
            }
         });

         $('#deleteBtn').on('click', function() {
            if (datarow != null) {
               var answer = confirm('Delete contact ' + datarow.firstName + '?');
               if (answer) {
                  // Call the controller method to delete a contact.
                  jQuery.ajax({
                     type: 'post',
                     url: '/contact/delete',
                     contentType: "application/json; charset=utf-8",
                     data: '{"contactID": "' + datarow.contactID + '"}',
                     complete: function(XHR, status) {
                        datarow = null;
                        $('#grid').jqxGrid('updatebounddata');
                     }
                  });
               }
            } else {
               toastr["warning"]('Please select a row to delete.', 'Contacts');
            }
         });

         $('#closeGridBtn').on('click', function() {
            $('#grid').jqxGrid('destroy');
            window.location = '/';
         });
      } // renderstatusbar
   });

   /* Load datarow with the selected row */
   $("#grid").bind('rowselect', function(event) {
      var row = event.args.rowindex;
      datarow = $("#grid").jqxGrid('getrowdata', row);
   });

   
   $('#nutritionCase').on('change', function() {
      var value = $('option:selected', this).val();
      if (value == 2) {
         // SAM
         $('#sam').removeClass('d-none');
         $('#cleft').addClass('d-none');
      }
      else {
         if (value == 3) {
            // Cleft/Oral structure abnormality
            $('#cleft').removeClass('d-none');
            $('#sam').addClass('d-none');
         }
         else {
            $('#sam').addClass('d-none');
            $('#cleft').addClass('d-none');
         }
      }
   });

   
   $('#submitRegisterForm').on('click', function() {
      // ToDo: save the form content.
      $('#registerPatient').removeClass('active');
      $('#patientsGrid').addClass('active');
   });


   $('#closeRegisterForm').on('click', function() {
      $('#registerPatient').removeClass('active');
      $('#patientsGrid').addClass('active');
   });


   // Adjust the height of the grid to 80% of the viewport.
   function resizeGrid() {
      viewPortHeight = window.visualViewport.height;
      viewPortHeight = viewPortHeight * 80 / 100;
      $('#grid').jqxGrid({ height: viewPortHeight });
   }
   window.onresize = resizeGrid;

});