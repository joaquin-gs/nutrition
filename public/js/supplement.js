$(() => {
   $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
   });

   //toastr.options.timeOut = 0;
   //toastr.options.extendedTimeOut = 0;
   toastr.options.closeButton = true;
   toastr.options.closeHtml = '<button><i class="fas fa-window-close"></i></button>';

   let currentInput = 0;
   let inputId = '';
   let inputs = [];   // Array of HTML input elements
   $("input[type=text]").each(function(i, el) {
      inputId = $(this).attr('id');
      inputs.push(inputId);
   });


   // Moves the focus from one input to the next
   // when ENTER key is pressed.
   $(document).on('keyup', 'input', function(e) {
      if (e.keyCode == 13) {
         e.preventDefault();
         currentInput++;
         if (currentInput < 15) {
            inputId = inputs[currentInput];
            $("#"+inputId).focus();
         }
      }
   });


   $("input[type=text]").on('click', function(event) {
      event.preventDefault();
      var id = $(this).attr('id');
      currentInput = inputs.indexOf(id);
   });


   var today = new Date();
   var weekNumber = getWeekNumber(today);
   var khmerDays = ['ច័ន្ទ', 'អង្គារ', 'ពុធ', 'ព្រហស្បតិ៍', 'សុក្រ', 'សៅរ៍', 'អាទិត្យ'];
   var patientID = '';
   var khmerName = '';
   var EnglishName = '';
   var weekYear = weekNumber + today.getFullYear().toString();
   var weekDay = today.getDay();
   var dayName = khmerDays[weekDay];
   var patientDoB = '';
   var gender = '';


   // Retrieve and display the menu of the current day.
   $.get('/menu/getMenu', { weekYear: weekYear, dayName: dayName }, function(response) {
      var menu = response[0];
      if (menu !== undefined) {
         row = `<tr>
                <td>${isEmpty(menu.meat) ? "" : menu.meat}</td> 
                <td>${isEmpty(menu.veggies) ? "" : menu.veggies}</td> 
                <td>${isEmpty(menu.rice) ? "" : menu.rice}</td> 
                <td>${isEmpty(menu.eggs) ? "" : menu.eggs}</td> 
                <td>${isEmpty(menu.oil) ? "" : menu.oil}</td> 
                <td>${isEmpty(menu.sugar) ? "" : menu.sugar}</td> 
                <td>${isEmpty(menu.hygiene) ? "" : menu.hygiene}</td>
            </tr>`;
         $('#menu table tbody').append(row);
      }
   });


   $('#patientID').on('blur', function() {
      patientID = $(this).val();
      if (patientID !== '') {
         $('#saveBtn').attr('disabled', false);
         $.get('/supplement/getPatient', { patientID: patientID },
            function(response) {
               //console.log(response);
               $('#saveBtn').attr('disabled', false);
               if (response !== "") {
                  if (response.hasFoodSupport == 'Y') {
                     toastr.warning(`<h6>The patient with ID: ${patientID} already has food support.</h6>`);
                     $('#saveBtn').attr('disabled', true);
                  }
                  else {
                     if (response.fullName.indexOf(",") > -1) {
                        khmerName = response.fullName.substr(0, response.fullName.indexOf(","));
                        EnglishName = response.fullName.substr(response.fullName.indexOf(",") + 1);
                        gender = response.gender;
                        patientDoB = response.dob;
                     }
                     else {
                        khmerName = response.fullName;
                        $("#myModal").modal('show');
                     }
                  }
                  // Update patient name label.
                  $('#patientName').text(khmerName).attr('title', EnglishName);
               }
            }
         );
      }
      else {
         $('#saveBtn').attr('disabled', true);
      }
   });

   
   $('#acceptData').on('click', function() {
      khmerName = $('#khmerName').val();
      EnglishName = $('#englishName').val();
      $('#patientName').text(khmerName).attr('title', EnglishName);
      // Validate date of birth
      var year = $("#year").val();
      var month = $("#month").val();
      var day = $("#day").val();
      if (year) {
         var currentYear = today.getFullYear();
         if (year < (currentYear-16) || year > currentYear) {
            toastr.warning('<h6>Year of birth is incorrect.</h6>');
            $("#year").css('border-color', '#e14a58');
            return false;
         }
         else {
            $("#year").css('border-color', '#ced4da');
         }
      }
      if (year && month) {
         if (month < 1 || month > 12) {
            toastr.warning('<h6>Month is incorrect.</h6>');
            $("#month").css('border-color', '#e14a58');
            return false;
         }
         else {
            $("#month").css('border-color', '#ced4da');
         }
      }
      if (year && month && day) {
         var daysInMonth = new Date(year, month, 0).getDate();
         if (day < 1 || day > daysInMonth) {
            toastr.warning('<h6>The day is incorrect.</h6>');
            $("#day").css('border-color', '#e14a58');
            return false;
         }
         else { 
            $("#day").css('border-color', '#ced4da');
            patientDoB = year + '-' + month + '-' + day;
         }
      }
      $("#closeModal").trigger('click');
   });


   // Store data into the database.
   $('#saveBtn').on('click', function() {
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      gender = gender == '' ? $('#sex option:selected').val() : gender;
      $.ajax({
         type: 'post',
         url: '/supplement/store',
         data: {
            donationDate: yyyy+'-'+mm+'-'+dd,
            patientID: patientID,
            numPackages: $('#packages').val(),
            gender: gender,
            dob: patientDoB,
            sugar: $('#sugar').val(),
            rice: $('#rice').val(),
            pork: $('#pork').val(),
            beef: $('#beef').val(),
            chicken: $('#chicken').val(),
            fish: $('#fish').val(),
            egg: $('#eggs').val(),
            cannedFish: $('#cannedFish').val(),
            oil: $('#oil').val(),
            veggies: $('#veggies').val(),
            hygiene: $('#hygiene').val(),
            patientNameKh: khmerName,
            patientNameEn: EnglishName
         },
         success: function(response) {
            console.log(response);
            if (response === "") {
               $('input').val('');   // Clear all input fields.
               toastr.success('<h6>Patient was added to food support list</h6>');
            }
            else {
               if (response.indexOf("1062 Duplicate entry") > 0) {
                  toastr.warning(`<h6>Patient ${ patientID } already has food support.</h6>`);
                  $('#patientID').val('').focus();
               }
               else {
                  toastr.warning(response);
               }
            }
            $('#patientName').text('');
         }
      });
      patientID = '';
      currentInput = 0;
   });

   
   $('#cancelBtn').on('click', function() {
      window.location = '/';
   });
})