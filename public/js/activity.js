$(() => {

   $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
   });

   var source = {
      url: '/activity/getAll',
      datatype: "json",
      datafields: [
         { name: 'activityDate', type: 'date', format: 'MM/dd/yyyy' },
         { name: 'cooking', type: 'int' },
         { name: 'gardening', type: 'int' }
      ]
   }
   var dataAdapter = new $.jqx.dataAdapter(source);

   $('#jqxGrid').jqxGrid({
      source: dataAdapter,
      theme: 'darkblue',
      width: '100%',
      height: 400,
      showtoolbar: true,
      showstatusbar: true,
      statusbarheight: 30,
      showaggregates: true,
      columns: [
         { text: 'Date', dataField: 'activityDate', align: 'center', cellsformat: 'd', width: '20%' },
         {
            text: 'Cooking', datafield: 'cooking', cellsalign: 'right', width: '35%',
            aggregates: [
               {
                  '<b>Total</b>': function(aggregatedValue, currentValue, column, record) {
                     var total = parseInt(record['cooking']);
                     return aggregatedValue + total;
                  }
               }
            ]
         },
         { text: 'Gardening', datafield: 'gardening', cellsalign: 'right',
            aggregates: [
               {
                  '<b>Total</b>': function(aggregatedValue, currentValue, column, record)
                  {
                     var total = parseInt(record['gardening']);
                     return aggregatedValue + total;
                  }
               }
            ]
         }
      ],
      rendertoolbar: function(toolbar) {
         toolbar.append("<div><h5>Activities of this month</h5></div>");
      },
   });


   $('#saveBtn').on('click', function() {
      $.ajax({
         type: 'post',
         url: 'saveActivities',
         data: {
            cooking: $('#cooking').val(),
            gardening: $('#gardening').val(),
         },
         success: function(response) {
            if (response == 0) {
               toastr.warning('<h6>The activities could not be stored in the database.</h6>', 'Warning!');
            } else {
               toastr.success('<h6>The activities were saved.</h6>', 'Success!');
               // Reset the source property to refresh grid.
               $('#jqxGrid').jqxGrid({
                  'source': null
               });
               $('#jqxGrid').jqxGrid({
                  'source': dataAdapter
               });
            }
         },
         error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseJSON.message.includes("Duplicate entry")) {
               toastr.warning('<h6>Those activities already exist in the database.</h6>', 'Attention');
            } else {
               toastr.error('<h6>' + errorThrown + '</h6>', 'System problem');
            }
         }
      });
   });


   $('#cancelBtn').on('click', function() {
      window.location = '/';
   });

})