$(() => {
   $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
   });

   //toastr.options.timeOut = 0;
   //toastr.options.extendedTimeOut = 0;
   toastr.options.closeButton = true;
   toastr.options.closeHtml = '<button><i class="fas fa-window-close"></i></button>';

   var today = new Date();
   var englishDays = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
   var khmerDays = ['ច័ន្ទ', 'អង្គារ', 'ពុធ', 'ព្រហស្បតិ៍', 'សុក្រ', 'សៅរ៍', 'អាទិត្យ'];
   var weekDay = today.getDay()-1;
   var dayName = khmerDays[weekDay];


   $('#menuOfTheDay').text("Menu for " + englishDays[weekDay]);

   // Retrieve and display the menu of the current day.
   $.get('/menu/getMenu', {dayName: dayName}, function(response) {
      var menu = response[0];
      $('#saveBtn').attr('disabled', false);
      if (menu !== undefined) {
         row = `<tr>
                  <td>${isEmpty(menu.meat) ? "" : menu.meat}</td> 
                  <td>${isEmpty(menu.veggies) ? "" : menu.veggies}</td> 
                  <td>${isEmpty(menu.rice) ? "" : menu.rice}</td> 
                  <td>${isEmpty(menu.eggs) ? "" : menu.eggs}</td> 
                  <td>${isEmpty(menu.oil) ? "" : menu.oil}</td> 
                  <td>${isEmpty(menu.sugar) ? "" : menu.sugar}</td> 
                  <td>${isEmpty(menu.hygiene) ? "" : menu.hygiene}</td>
                </tr>`;
         $('#menu table tbody').append(row);
      }
      else {
         toastr.warning('<h6>The menu has not been created.</h6>');
         $('#saveBtn').attr('disabled', true);
      }
   });
   

   var currentDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate(); 
   // Retrieve the patients who requested food today
   // to build the body of the second table.
   $.get('/serving/getFoodSupportList', function(list) {
      //console.log(list);
      var row = '';
      for (var i = 0; i < list.length; i++) {
         row = `<tr>
                   <td>${i + 1}</td>
                   <td>${list[i].donationDate}</td>
                   <td class="patientID">${list[i].patientID}</td> 
                   <td class="patientNameKh" title="${list[i].patientNameEn}">${list[i].patientNameKh}</td> 
                   <td class="dob">${list[i].dob}</td> 
                   <td class="gender">${list[i].gender}</td> 
                   <td class="sugar">${isEmpty(list[i].sugar) ? "" : list[i].sugar}</td> 
                   <td class="rice">${isEmpty(list[i].rice) ? "" : list[i].rice}</td>
                   <td class="pork">${isEmpty(list[i].pork) ? "" : list[i].pork}</td>
                   <td class="beef">${isEmpty(list[i].beef) ? "" : list[i].beef}</td> 
                   <td class="chicken">${isEmpty(list[i].chicken) ? "" : list[i].chicken}</td> 
                   <td class="fish">${isEmpty(list[i].fish) ? "" : list[i].fish}</td> 
                   <td class="egg">${isEmpty(list[i].egg) ? "" : list[i].egg}</td> 
                   <td class="cannedFish">${isEmpty(list[i].cannedFish) ? "" : list[i].cannedFish}</td> 
                   <td class="oil">${isEmpty(list[i].oil) ? "" : list[i].oil}</td> 
                   <td class="veggies">${isEmpty(list[i].veggies) ? "" : list[i].veggies}</td> 
                   <td class="hygiene">${isEmpty(list[i].hygiene) ? "" : list[i].hygiene}</td> 
                   <td class="received" title="Click to check/uncheck"></td>
                   <td><button type="button" id="${list[i].patientID}" class="btn btn-outline-primary delete">Delete</button></td>
               </tr>`
         $('#list table tbody').append(row);
      }
   });


   // Check/uncheck the received column.
   $(document).on('click', 'td.received', function() {
      var td = $(this);
      if (td.children().length == 0) {
         td.append('<img src="/images/check.png" width=28 height=24>');
      }
      else {
         td.empty();
      }
   });


   // Removes a patient from the food support program.
   $(document).on('click', '.delete', function() {
      var patientID = $(this).attr('id');
      // Mark the patient as inactive.
      if (confirm('Do you want to remove this patient from the list?')) {
         $.post('/serving/removePatient', {patientID: patientID}, function(response) {
            console.log(response);
         });
         $(this).closest('tr').remove();
      }
   });


   // Update the received food record.
   $('#saveBtn').on('click', function() {
      var error = false;
      var checked = false;
      $('#list tbody tr').each(function(index, row) {
         checked = $('td.received', row).children().length > 0;
         
         if (checked) {
            $.ajax({
               type: 'post',
               url: '/serving/insert',
               data: {
                  patientID: $('td.patientID', row).text(),
                  donationDate: '',
                  sugar: $('td.sugar', row).text(),
                  rice: $('td.rice', row).text(),
                  pork: $('td.pork', row).text(),
                  beef: $('td.beef', row).text(),
                  chicken: $('td.chicken', row).text(),
                  fish: $('td.fish', row).text(),
                  egg: $('td.egg', row).text(),
                  cannedFish: $('td.cannedFish', row).text(),
                  oil: $('td.oil', row).text(),
                  veggies: $('td.veggies', row).text(),
                  hygiene: $('td.hygiene', row).text(),
                  officer: window.currentUser
               },
               success: function(response) {
                  //console.log('response: ' + response);
                  if (response == 0) {
                     response = 'Could not store data in database.';
                     if (!error) {
                        error = true;
                        toastr.error(`<h6>${ response }</h6>`);
                     }
                  }
               }
            });
         }
         
      });
      if (!error) {
         toastr.options.timeOut = 0;
         toastr.options.extendedTimeOut = 0;
         toastr.options.onCloseClick = function() {window.location = '/';};
         toastr.success('<h6>Data was saved!</h6>');
      }
   });


   $('#cancelBtn').on('click', function() {
      window.location = '/';
   });

})