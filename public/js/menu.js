/**
 * This file contains the common logic used by two different pages:
 * foodOrder and the menu builder.
 */
$(() => {
   $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
   });

   var tableRow = '';
   var tableCol = '';
   var rowNumber = -1;
   var colNumber = -1;
   var dialogCols = [1, 2, 3, 5, 6];

   var khmerDays = ['ច័ន្ទ', 'អង្គារ', 'ពុធ', 'ព្រហស្បតិ៍', 'សុក្រ', 'សៅរ៍', 'អាទិត្យ'];
   var EnglishDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
   var today = new Date();
   var currentPage = window.location.pathname;

   var weekDay = today.getDay();

   if (currentPage === "/menu/foodOrder") {
      // Get the list of patients who requested food support yesterday.
      today.setDate(today.getDate() - 1);  // Yesterday
      var weekDay = today.getDay();
      var dayName = khmerDays[weekDay];
      var numberOfPatients = 0;
      $.get('/serving/foodSupportPatients', function(response) {
         if (typeof (response) == 'object') {
            if (response.numPatients > 0) {
               numberOfPatients = response.numPatients;
               // Get the menu served yesterday
               $.get('/menu/getMenu', {dayName: dayName}, function(data) {
                  var menu = data[0];
                  var food = menu.meat.split(" ");
                  var qty = 0;
                  $('#meat td.foodType').text(food[0]);
                  qty = food[1] * numberOfPatients;
                  $('#meat td.qty').text(qty);
                  $('#meat td.measurement').text(food[2]);

                  food = menu.veggies.split(" ");
                  $('#veggies td.foodType').text(food[0]);
                  qty = food[1] * numberOfPatients;
                  $('#veggies td.qty').text(qty);
                  $('#veggies td.measurement').text(food[2]);

                  food = menu.rice.split(" ");
                  qty = food[0] * numberOfPatients;
                  $('#rice td.qty').text(qty);
                  $('#rice td.measurement').text(food[1]);

                  food = menu.eggs.split(" ");
                  qty = food[0] * numberOfPatients;
                  $('#egg td.qty').text(qty);

                  food = menu.oil.split(" ");
                  qty = food[0] * numberOfPatients;
                  $('#oil td.qty').text(qty);
                  $('#oil td.measurement').text(food[1]);

                  food = menu.sugar.split(" ");
                  qty = food[0] * numberOfPatients;
                  $('#sugar td.qty').text(qty);
                  $('#sugar td.measurement').text(food[1]);
               });
            }
         }
      });
   }
   else {
      // Generate the table body.
      for (var row = 0; row < khmerDays.length; row++) {
         tableRow = '<tr>';
         for (var col = 0; col < 8; col++) {
            tableCol = '<' + (col == 0 ? 'th class="colHeader" scope="row" title="' + EnglishDays[row] + '">' : 'td class="col' + (col) + '"');
            if (col > 0) {
               if (dialogCols.includes(col)) {
                  tableCol += ' data-toggle="modal" data-target="#mymodal">';
               } else {
                  if (col == 4 || col == 7) {
                     tableCol += "contenteditable='true'";
                  }
                  tableCol += '>';
               }
            }
            tableCol = tableCol + (col == 0 ? khmerDays[row] + '</th>' : '</td>');
            tableRow = tableRow + tableCol;
         }
         tableRow = tableRow + '</tr>';
         $('table tbody').append(tableRow);
      }


      // Open modal dialog to capture data for the current table row.
      $('table').on('click', '.col1, .col2, .col3, .col5, .col6', function() {
         rowNumber = $(this).closest('tr').index();
         $("#body-content").empty();

         // Copy and show the appropriate HTML block into the dialog.
         if ($(this).hasClass('col1')) {
            $('.modal-title').text("សាច់");
            $('#body-content').append($('#meat').clone());
            $('#modal-body #meat').removeClass('hidden');
            colNumber = 0;
         }
         if ($(this).hasClass('col2')) {
            $('.modal-title').text("បន្លែ");
            $('#body-content').append($('#veggies').clone());
            $('#modal-body #veggies').removeClass('hidden');
            colNumber = 1;
         }
         if ($(this).hasClass('col3')) {
            $('.modal-title').text("អង្ករ");
            $('#body-content').append($('#rice').clone());
            $('#modal-body #rice').removeClass('hidden');
            colNumber = 2;
         }
         if ($(this).hasClass('col5')) {
            $('.modal-title').text("ប្រេងសណ្ដែក");
            $('#body-content').append($('#oil').clone());
            $('#modal-body #oil').removeClass('hidden');
            colNumber = 4;
         }
         if ($(this).hasClass('col6')) {
            $('.modal-title').text("ស្ករស");
            $('#body-content').append($('#sugar').clone());
            $('#modal-body #sugar').removeClass('hidden');
            colNumber = 5;
         }

         // Get table cell text from the current row and col.
         var cellValue = $("tbody tr:eq(" + rowNumber + ") td:eq(" + colNumber + ")").text();

         // Split table cell value into an array.
         var cellContent = cellValue.split(" ");

         // Check length of the array.
         if (cellContent.length == 4) {
            cellContent = cellContent[0] + ' ' + cellContent[1];
            $(".qty").val(1);
         } else {
            if (colNumber < 2) {
               $(".qty").val(cellContent[1]);
            } else {
               $(".qty").val(cellContent[0]);
            }
         }

         // Update dialog dropdown list.
         if (colNumber == 1) {
            $("#meatList").val(cellContent[0]);
         }
         if (colNumber == 2) {
            $("#veggiesList1").val(cellContent[0]);
         }
         if (colNumber == 3) {
            $("#rice").val(cellContent[0] + " គ.ក");
         }
      });


      $('#mymodal').on('shown.bs.modal', function (e) {
         $('#mymodal *:input[type=text]:first').focus();
      })
      
      // Set the quantity for each type of meat.
      $(document).on('click', '#meatList', function() {
         var value = $("#meatList option:selected").val();
         if (value == 'Canned fish') {
            $(".qty").val(1);
         } else {
            $(".qty").val('');
         }
      });


      // Moves data from dialog input fields to the current HTML table row.
      $('#acceptData').on('click', function() {
         var value = "";
         if (colNumber == 0) {   // meat
            value = $("#meatList option:selected").text();
            value += ' ' + $(".qty").val() + ' ' + (value == 'ត្រីកំប៉ុង' ? 'កំប៉ុង' : 'ខាំ'); 
         }
         if (colNumber == 1) {   // veggies
            value = $("#veggiesList1 option:selected").val();
            optText = $("#veggiesList1 option:selected").text();
            value = optText + " " + (["Onion", "Cucumber", "Tomato"].includes(value) ? $(".qty1").val() + ' ' : $(".qty1").val() + ' ខាំ');

            if ($(".qty2").val() !== '') {
               var value2 = $("#veggiesList2 option:selected").val();
               var optText2 = $("#veggiesList2 option:selected").text();
               value += optText2 + ' ' + (["Onion", "Cucumber", "Tomato"].includes(value2) ? $(".qty2").val() : $(".qty2").val() + ' ខាំ');
            }
         }
         if (colNumber == 2) {   // rice
            value = $("#rice .qty").val();
            if (!isEmpty(value)) {
               value += " គ.ក";
            }
         }
         if (colNumber == 4) {   // oil
            value = $("#oil .qty").val();
            if (!isEmpty(value)) {
               value += " ខាំ";
            }
         }
         if (colNumber == 5) {   // sugar
            value = $("#sugar .qty").val();
            if (!isEmpty(value)) {
               value += " ខាំ";
            }
         }
         $("tbody tr").eq(rowNumber).children('td').eq(colNumber).text(value);
         $("#closeModal").trigger('click');
      });


      // Send data to the database.
      $('#saveBtn').on('click', function() {
         var struct = {
            dayName: '',
            meat: '',
            veggies: '',
            rice: '',
            eggs: '',
            oil: '',
            sugar: '',
            hygiene: ''
         }
         var colValue = '';
         $('table tbody tr').each(function(index, row) {
            struct.dayName = khmerDays[index];
            $(row).children('td').each(function(i, col) {
               colValue = $(col).text();
               switch (i) {
                  case 0:
                     struct.meat = colValue;
                     break;
                  case 1:
                     struct.veggies = colValue;
                     break;
                  case 2:
                     struct.rice = colValue;
                     break;
                  case 3:
                     struct.eggs = colValue;
                     break;
                  case 4:
                     struct.oil = colValue;
                     break;
                  case 5:
                     struct.sugar = colValue;
                     break;
                  case 6:
                     struct.hygiene = colValue;
               }
            });

            $.ajax({
               type: 'post',
               url: '/menu/store',
               async: false,
               data: struct,
               success: function(response) {
                  console.log(response);
               }
            });
         });
         toastr.success('<h6>Menu saved.</h6>');
      });


      // Show the menu if it is already stored in the database.
      $.get('/menu/getMenu', {dayName: ''}, function(data) {
         if (data.length > 0) {
            // Populate the table with the current data.
            data.forEach(function(item, row) {
               $("table tbody tr:eq(" + row + ") td:eq(0)").text(item.meat);
               $("table tbody tr:eq(" + row + ") td:eq(1)").text(item.veggies);
               $("table tbody tr:eq(" + row + ") td:eq(2)").text(item.rice);
               $("table tbody tr:eq(" + row + ") td:eq(3)").text(item.eggs);
               $("table tbody tr:eq(" + row + ") td:eq(4)").text(item.oil);
               $("table tbody tr:eq(" + row + ") td:eq(5)").text(item.sugar);
               $("table tbody tr:eq(" + row + ") td:eq(6)").text(item.hygiene);
            });
         }
      });
   }

   
   $('#cancelBtn').on('click', function() {
      window.location = '/';
   });

})