<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;

class notifyRegistration {
   /**
    * Create the event listener.
    *
    * @return void
    */
   public function __construct() {
      //
   }


   /**
    * Handle the event.
    *
    * @param  object  $event
    * @return void
    */
   public function handle(Registered $event) {
      // Get registered user data:
      $userName = $event->user['name'];
      $userEmail = $event->user['email'];

      // Telegram ID for Joaquin.
      $chatId = '901061200';
      $msg = "The user " . $userName . " (" . $userEmail . "), has registered into the Nutrition App. Assign the user a role.";

      $botToken = '5083299367:AAFDXf7THnkJhO6EUY7bUgtHXOQsbNEAVpk';
      $website = 'https://api.telegram.org/bot' . $botToken;
      $url = $website . '/sendMessage';
      $data = array('chat_id' => $chatId, 'text' => $msg);
      $options = array('http' => array('method' => 'POST', 'header' => "Content-Type:application/x-www-form-urlencoded\r\n", 'content' => http_build_query($data)));
      $context = stream_context_create($options);
      file_get_contents($url, false, $context);
   }
}
