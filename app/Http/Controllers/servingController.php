<?php

namespace App\Http\Controllers;

use App\Models\serving;
use Illuminate\Http\Request;

class servingController extends Controller {
   
   private $serving;

   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct() {
      $this->middleware('auth');
      $this->serving = new serving();
   }


   /**
    * Render the servings view.
    *
    * @return \Illuminate\Http\Response
    */
   public function index() {
      return view('serving/index');
   }


   /**
    * Returns the list of patients with food support.
    * @return array
    */
   public function getFoodSupportList(Request $request) {
      //$donationDate = $request->input('donationDate');
      return $this->serving->getFoodSupportList();
   }


   /**
    * Returns the number of patients that got food support yesterday.
    * @return int $result
    */
   public function foodSupportPatients() {
      $result = $this->serving->foodSupportPatients();
      return $result;
   }


   /**
    * Store a newly created resource in serving table.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function insert(Request $request) {
      $result = $this->serving->insertRow($request->all());
      return $result;
   }


   public function removePatient(Request $request) {
      $patientID = $request->input('patientID');
      return $this->serving->removePatient($patientID);
   }


   public function monthlyConsumptionReport() {
      $servings = $this->serving->monthlyConsumptionReport();
      if ($servings == 0) {
         $servings = array_fill(0, 1, (object) ["dayOfMonth"=>"", "numPatients"=>"0"]);
      }
      return view('serving/monthlyReport', ['servings'=>$servings]);
   }
}
