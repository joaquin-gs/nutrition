<?php

namespace App\Http\Controllers;

use App\Models\patient;
use Illuminate\Http\Request;

class patientController extends Controller {

   private $patient;

   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct() {
      $this->middleware('auth');
      $this->patient = new patient();
   }


   public function index() {
      return view('patient/index');
   }


   public function getAllPatients() {
      return $this->patient->getAllPatients();
   }


   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
   public function register() {
      return view('patient/register');
   }
}
