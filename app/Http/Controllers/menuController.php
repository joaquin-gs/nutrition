<?php

namespace App\Http\Controllers;

use App\Models\menu;
use Illuminate\Http\Request;

class menuController extends Controller {
   private $menu;


   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct() {
      $this->middleware('auth');
      $this->menu = new menu();
   }


   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index() {
      return view('menu/index');
   }


   public function createFoodOrder(Request $request) {
      return view('menu/foodOrder');
   }


   /**
    * Returns an array containing the menu for that date.
    * @param Request $request
    * @return array $menu
    */
   public function getMenu(Request $request) {
      $fields = $request->all();
      $menu = $this->menu->getMenu($fields['dayName']);
      return $menu;
   }

   
   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return string
    */
   public function store(Request $request) {
      $fields = $request->all();
      return $this->menu->store($fields);
   }
}
