<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
      $this->middleware('auth');
   }


   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
   public function index()
   {
      $user = Auth::user();
      if ($user->role == "None") {
         return view('needApproval');
      } 
      else {
         return view('home');
      }
   }


   public function assignRole() {
      $aRoles[] = (object) array('roleID' => 'Admin', 'roleName' => 'Admin');
      $aRoles[] = (object) array('roleID' => 'NUM', 'roleName' => 'Nutrition manager');
      $aRoles[] = (object) array('roleID' => 'NUA', 'roleName' => 'Nutrition assistant');
      $aRoles[] = (object) array('roleID' => 'SW', 'roleName' => 'Social worker');
      $aRoles[] = (object) array('roleID' => 'None', 'roleName' => 'Unassigned');

      // Retrieve users and their role, if any.
      $users = DB::select('SELECT u.id, u.name, u.role FROM users u');

      return view('assignRole', ["roles"=>$aRoles, "users"=>$users]);
   }


   public function saveRoles(Request $request) {
      $arrUserRoles = $request->roles;
      for ($i = 0; $i < count($arrUserRoles); $i++) {
         $userId = $arrUserRoles[$i]['userID'];
         $roleId = $arrUserRoles[$i]['roleID'];
         if ($roleId != '0') {
            DB::table('users')->where('id', $userId)->update(array('role' => $roleId));
         }
      }
   }
}
