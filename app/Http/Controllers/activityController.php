<?php

namespace App\Http\Controllers;

use App\Models\activity;
use Illuminate\Http\Request;

class activityController extends Controller {

   private $activity;

   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct() {
      $this->middleware('auth');
      $this->activity = new activity;
   }


   /**
    * Show the activities grid to be filled in.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
   public function addActivity() {
      return view('activity/addActivity');
   }


   public function getAllActivities() {
      return $this->activity->getAllActivities();
   }


   public function saveActivities(Request $request) {
      $cooking = $request->input('cooking');
      $gardening = $request->input('gardening');
      return $this->activity->saveActivities($cooking, $gardening);
   }
}
