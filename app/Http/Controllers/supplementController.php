<?php

namespace App\Http\Controllers;

use App\Models\supplement;
use Illuminate\Http\Request;

class supplementController extends Controller {
   private $supplement;

   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct() {
      $this->middleware('auth');
      $this->supplement = new supplement();
   }

   
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index() {
      return view('supplement/index');
   }


   /**
    * Retrieves data of the given patient ID.
    * 
    * @param string patientID 
    * @return string 
    */
   public function getPatient(Request $request) {
      $dob = "";
      $sex = "";
      $patientID = $request->input("patientID");
      $hasSupport = $this->supplement->foodSupportStatus($patientID);

      // Look for patient id in HIS.
      $patient = $this->supplement->getPatient($patientID);
      if (!empty($patient)) {
         $fullName = $patient[0]->family_name_khmer . " " . $patient[0]->first_name_khmer . "," . $patient[0]->family_name_english . " " . $patient[0]->first_name_english;
         $sex = $patient[0]->sex;
         $dob = $patient[0]->date_birth;
      }
      else {
         $fullName = "Patient not found in HIS database.";
      }
      return ["fullName"=>$fullName, "hasFoodSupport"=> $hasSupport, "dob"=>$dob, "gender"=>$sex];
   }


   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request) {
      $fields = $request->all();
      return $this->supplement->store($fields);
   }
 
}
