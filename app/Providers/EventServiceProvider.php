<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Listeners\notifyRegistration;

class EventServiceProvider extends ServiceProvider {
   /**
    * The event listener mappings for the application.
    *
    * @var array<class-string, array<int, class-string>>
    */
   protected $listen = [
      Registered::class => [notifyRegistration::class]
   ];


   /**
    * Register any events for your application.
    *
    * @return void
    */
   public function boot() {
      //
   }
}
