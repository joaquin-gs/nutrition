<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
   /**
    * The policy mappings for the application.
    *
    * @var array<class-string, class-string>
    */
   protected $policies = [
      // 'App\Models\Model' => 'App\Policies\ModelPolicy',
   ];

   /**
    * Register any authentication / authorization services.
    *
    * @return void
    */
   public function boot() {
      $this->registerPolicies();

      Gate::define('admin', function (User $user) {
         return $user->role === 'admin' || $user->name == 'Joaquin';
      });

      Gate::define('nutrition', function (User $user) {
         return $user->role === 'NUA' || $user->name == 'Joaquin';
      });

      Gate::define('nutrition_manager', function (User $user) {
         return $user->role === 'NUM' || $user->name == 'Joaquin';
      });

      Gate::define('sw-user', function (User $user) {
         return $user->role === 'SW' || $user->name == 'Joaquin';
      });
   }
}
