<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class create extends Command
{
   /**
    * The name and signature of the console command.
    *
    * @var string
    */
   protected $signature = 'create:model {tableName} {--m|migration}';


   /**
    * The console command description.
    *
    * @var string
    */
   protected $description = 'Creates a new model class.';


   /**
    * Create a new command instance.
    *
    * @return void
    */
   public function __construct()
   {
      parent::__construct();
   }


   /**
    * Execute the console command.
    *
    * @return int
    */
   public function handle()
   {
      // Variables 
      $tableName = $this->argument('tableName');
      $createMigration = $this->option('migration');
      $autoInc = false;
      $intKey = false;

      if (empty($tableName) || is_null($tableName)) {
         $this->error('Table name not especified.');
         return 0;
      }

      // Retrieve table structure from schema.
      $query = "SELECT column_name, 
                       is_nullable, 
                       data_type, 
                       column_type,
                       character_maximum_length,
                       column_key,
                       extra
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = ?
                  AND table_name = ?";
      $tableStructure = DB::select($query, [env('DB_DATABASE'), $tableName]);
      if (empty($tableStructure)) {
         $this->error('Table name not found in ' . \env('DB_DATABASE'));
         return 0;
      }

      // Find the primary key
      $key = array_search('PRI', array_column($tableStructure, 'column_key'));
      $primaryKey = $tableStructure[$key]->column_name;
      $intKey = in_array($tableStructure[$key]->data_type, ['tinyint', 'smallint', 'mediumint', 'int', 'bigint']);
      $autoInc = $tableStructure[$key]->extra === 'auto_increment';

      // Begin writting PHP model class file
      $content = "<?php" . PHP_EOL . PHP_EOL . 'namespace App\Models;' . PHP_EOL . PHP_EOL;
      $content .= "use App\Models\baseModel;" . PHP_EOL . PHP_EOL;
      $content .= "use Illuminate\Database\Eloquent\Model;" . PHP_EOL . PHP_EOL;
      $content .= "class " . $tableName . " extends baseModel" . PHP_EOL;
      $content .= "{" . PHP_EOL;
      $content .= '   protected $table = "' . $tableName . '";' . PHP_EOL;
      $content .= '   protected $primaryKey = "' . $primaryKey . '";' . PHP_EOL;
      if ($intKey) {
         $content .= '   public $incrementing = ' . ($autoInc ? 'true' : 'false') . ';' . PHP_EOL . PHP_EOL;
      }
      else {
         $content .= '   protected $keyType = "string";' . PHP_EOL . PHP_EOL;
      }

      // The fillable columns array and its Validation Rules.
      $rules = '';
      $content .= '   protected $fillable = [' . PHP_EOL;
      $columns = '';
      $keys = '';
      foreach ($tableStructure as $key => $value) {
         $charLen = $value->character_maximum_length;
         if (in_array($value->column_key, ['MUL', 'PRI'])) {
            $keys .= '$table->primary(\'' . $value->column_name . '\')';
            if (!$autoInc) {
               $keys .= "->autoIncrement()";
               $content .= str_repeat(' ', 6) . '"' . $value->column_name . '",' . PHP_EOL;
               if ($intKey) {
                  $rules .= str_repeat(' ', 6) . '"' . $value->column_name . '" => ' . '"required|numeric",' . PHP_EOL;
               } 
               else {
                  $rules .= str_repeat(' ', 6) . '"' . $value->column_name . '" => ' . '"required|alpha_num|max:' . $charLen . '",' . PHP_EOL;
               }
            }
            $columns .= PHP_EOL;
         } 
         else {  // all the other columns
            $content .= str_repeat(' ', 6) . '"' . $value->column_name . '",' . PHP_EOL;

            // Build validation rules...
            $required = $value->is_nullable === 'NO';
            $max = !empty($charLen) ? "max:" . $charLen : "";
            $colType = '';
            $isUnsigned = str_contains($value->column_type, 'unsigned');

            if ($value->data_type == 'date') {
               $colType .= 'date';
               $columns .= '$table->date(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'datetime') {
               $colType .= 'date_format:Y-m-d H:i:s';
               $columns .= '$table->dateTime(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            } 
            if ($value->data_type == 'char') {
               $colType .= 'alpha_num';
               $columns .= '$table->char(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'varchar') {
               $colType .= 'alpha_num';
               $columns .= '$table->string(\'' . $value->column_name . '\', '.$charLen.')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'tinyint') {
               $colType .= 'Numeric';
               $columns .= '$table->tinyInteger(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'smallint') {
               $colType .= 'Numeric';
               $columns .= '$table->smallInteger(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'mediumint') {
               $colType .= 'Numeric';
               $columns .= '$table->mediumInteger(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'int') {
               $colType .= 'Numeric';
               $columns .= '$table->integer(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'bigint') {
               $colType .= 'Numeric';
               $columns .= '$table->bigInteger(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            if ($value->data_type == 'float') {
               $colType .= 'Numeric';
               $columns .= '$table->float(\'' . $value->column_name . '\')' . $required ? ';' : '->nullable();';
            }
            $str = '"' . $colType . ($required ? '|required' : '') . ($max !== "" ? '|' . $max : '') . '"';
            $rules .= str_repeat(' ', 6) . '"' . $value->column_name . '" => ' . $str . ',' . PHP_EOL;
         }
      } // foreach
      $content .= '   ];' . PHP_EOL . PHP_EOL;

      // Model validation rules, based on columns constraints (required/max length).
      $content .= '   protected $rules = array(' . PHP_EOL;
      $content .= $rules;
      $content .= '   );' . PHP_EOL;
      $content .= '}';

      $currDir = getcwd();
      if (!file_exists($currDir . '/app/Models')) {
         mkdir($currDir . '/app/Models', 0777, true);
      }

      $file = $currDir . '/app/Models/' . $tableName . '.php';
      $result = file_put_contents($file, $content);
      if ($result === false) {
         echo "Could not create class file.";
         return 0;
      }

      if ($createMigration) {
         $content = "<?php" . PHP_EOL . PHP_EOL ;
         $content .= "use Illuminate\Database\Migrations\Migration;" . PHP_EOL;
         $content .= "use Illuminate\Database\Schema\Blueprint;" . PHP_EOL;
         $content .= "use Illuminate\Support\Facades\Schema;" . PHP_EOL;
         $content .= "class create" . $tableName . "Table extends Migration" . PHP_EOL . PHP_EOL;
         $content .= "{" . PHP_EOL;
         $content .= "   /**" . PHP_EOL;
         $content .= "    * Run the migrations." . PHP_EOL;
         $content .= "    *" . PHP_EOL;
         $content .= "    * @return void" . PHP_EOL;
         $content .= "    */" . PHP_EOL;
         $content .= "   public function up() {" . PHP_EOL;
         $content .= "      Schema::create('feed', function (Blueprint $tableName) {". PHP_EOL;
         $content .= $columns;
         $content .= "      });". PHP_EOL;
         $content .= "   }". PHP_EOL;
         $content .= "". PHP_EOL;
         echo "Created Migration: .fecha.hora._my_table_name";
      }
      $this->info('The command was successful!');
   }
}
/*
if table key is compound:
   $table->primary(['id', 'parent_id']);

->default($value)
->nullable($value = true)

$table->unsignedInteger('votes');
*/