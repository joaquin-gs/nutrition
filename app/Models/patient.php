<?php

namespace App\Models;

use App\Models\baseModel;
use Illuminate\Support\Facades\DB;

class patient extends baseModel {
   protected $table = "patient";
   protected $primaryKey = "patientID";
   protected $keyType = "string";

   protected $fillable = [
      "patientID",
      "patientNameEn",
      "patientNameKh",
      "referredBy",
      "initialWeight",
      "diagnosis",
      "motherAge",
      "numberOfChildren",
      "remarks",
      "hasHIV",
      "nutritionCase",
      "createdOn",
      "createdBy",
      "activeFoodSupport",
      "lastSeen",
   ];

   protected $rules = array(
      "patientID" => "required|string|max:11",
      "patientNameEn" => "string|max:45|nullable",
      "patientNameKh" => "string|max:45|nullable",
      "referredBy" => "string|max:50|nullable",
      "initialWeight" => "Numeric|nullable",
      "diagnosis" => "string|max:10|nullable",
      "motherAge" => "Numeric|nullable",
      "numberOfChildren" => "Numeric|nullable",
      "remarks" => "string|max:400|nullable",
      "hasHIV" => "string|max:1|nullable",
      "nutritionCase" => "Numeric|nullable",
      "createdOn" => "date_format:Y-m-d H:i:s|nullable",
      "createdBy" => "string|max:50|nullable",
      "activeFoodSupport" => "string|required|in:Y,N",
      "lastSeen" => "date|nullable",
   );


   public function getAllPatients() {
      $query = "select p.patientID,
                       p.patientNameKh,
                       p.patientNameEn,
                       p.referredBy,
                       p.initialWeight,
                       p.diagnosis,
                       p.motherAge,
                       p.numberOfChildren,
                       p.remarks,
                       p.hasHIV,
                       p.nutritionCase,
                       g.date_birth,
                       g.sex,
                       s.province_name_khmer,
                       d.district_name_khmer,
                       c.commune_name_khmer,
                       v.village_name_khmer,
                       g.phone1,
                       p.activeFoodSupport,
                       p.createdOn
                 from patient p
                   join his.patients g on (g.patient_code = p.patientID)
                   join his.provinces s on (s.province_code = g.province_code)
                   join his.districts d on (d.district_code = g.district_code)
                   join his.communes c on (c.commune_code = g.commune_code)
                   join his.villages v on (v.village_code = g.village_code)";
      return DB::select($query);
   }
}