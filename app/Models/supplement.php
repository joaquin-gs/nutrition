<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\baseModel;
use Illuminate\Support\Facades\Auth;
use App\Models\patient;
use App\Models\serving;

use function PHPUnit\Framework\isNull;

class supplement extends baseModel {
   protected $table = "supplement";
   protected $primaryKey = "donationDate";
   protected $keyType = "date";
   private $patient;
   private $serving;

   protected $fillable = [
      "patientID",
      "donationDate",
      "numPackages",
      "sugar",
      "rice",
      "pork",
      "beef",
      "chicken",
      "fish",
      "egg",
      "cannedFish",
      "oil",
      "veggies",
      "hygiene",
   ];

   protected $rules = array(
      "patientID" => "required|string|max:11",
      "donationDate" => "required|date",
      "numPackages" => "integer",
      "sugar" => "integer",
      "rice" => "integer",
      "pork" => "integer",
      "beef" => "integer",
      "chicken" => "integer",
      "fish" => "integer",
      "egg" => "integer",
      "cannedFish" => "integer",
      "oil" => "integer",
      "veggies" => "string|max:45|nullable",
      "hygiene" => "integer",
   );


   /**
    * Retrieves the patient's name of a given patient ID from the HIS database.
    * Returns an array if patientID is found or empty string otherwise.
    * @param string patientID 
    * @return string|array $result
    */
   public function getPatient($patientID) {
      $result = "";
      $patient = DB::connection('his')->table('patients')->where('patient_code', $patientID)->get(['family_name_english', 'first_name_english', 'family_name_khmer', 'first_name_khmer', 'date_birth', 'sex']);
      if ($patient->isNotEmpty()) {
         $result = $patient->toArray();
      }
      return $result;
   }


   /**
    * Returns the value of the column patient.activeFoodSupport.
    * The value of $status can be 'Y', 'N' or an empty 
    * string, in case the patient is not found.
    * @param string $patientID
    * @return string $status
    */
   public function foodSupportStatus($patientID) {
      $status = DB::select("SELECT activeFoodSupport FROM patient WHERE patientID = ?", [$patientID]);
      $status = count($status) == 0 ? 'N' : $status[0]->activeFoodSupport;
      return $status;
   }


   /**
    * Stores the supplementary food in the database.
    * Returns true if successful or a string with error details otherwise.
    * @param array $fields 
    * @returns boolean|string $result
    */
   public function store($fields) {
      $result = '';
      try {
         $this->patient = new patient();
         $patient = array(
            'patientID' => $fields['patientID'],
            'patientNameEn' => $fields["patientNameEn"],
            'patientNameKh' => $fields["patientNameKh"],
            'gender' => $fields["gender"],
            'dob' => $fields["dob"],
            'activeFoodSupport' => "Y",
            'createdOn' => date("Y-m-d H:i:s"),
            'createdBy' => Auth::user()->name
         );
         if ($this->patient->validate($patient)) {
            // Add patient to the nutrition DB.
            if (!$this->patient->find($fields['patientID'])) {
               DB::table('patient')->insert($patient);
            }
         }
         else {
            $result = $this->patient->errors();
         }

         // Creates the "digital ticket" for the patient
         // to claim the food.
         $this->serving = new serving();
         $servingData = array(
            'donationDate'=>$fields['donationDate'], 
            'patientID'=>$fields['patientID'], 
            'officer'=> "Not set", 
         );
         if ($this->serving->validate($servingData)) {
            DB::table('serving')->insert($servingData);
         }
         else {
            $result .= $this->serving->errors();
         }

         // Stores the additional food for the patient, if 
         // there is any.
         $supplement = array(
            'donationDate' => $fields['donationDate'],
            'patientID' => $fields['patientID'],
            'numPackages' => $fields['numPackages'],
            'sugar' => isNull($fields['sugar']) ? 0 : $fields['sugar'],
            'rice' => isNull($fields['rice']) ? 0 : $fields['rice'],
            'pork' => isNull($fields['pork']) ? 0 : $fields['pork'],
            'beef' => isNull($fields['beef']) ? 0 : $fields['beef'],
            'chicken' => isNull($fields['chicken']) ? 0 : $fields['chicken'],
            'fish' => isNull($fields['fish']) ? 0 : $fields['fish'],
            'egg' => isNull($fields['egg']) ? 0 : $fields['egg'],
            'cannedfish' => isNull($fields['cannedFish']) ? 0 : $fields['cannedFish'],
            'oil' => isNull($fields['oil']) ? 0 : $fields['oil'],
            'veggies' => isNull($fields['veggies']) ? ' ' : $fields['veggies'],
            'hygiene' => isNull($fields['hygiene']) ? 0 : $fields['hygiene'],
         );
         if ($this->validate($supplement)) {
            $str = '';
            foreach ($supplement as $key => $value) {
               if ($key !== "donationDate" || $key !== "patientID") {
                  $str .= $value;
               }
            }
            if ($str !== "") {
               DB::table($this->table)->insert($supplement);
            }
         }
         else {
            $result .= $this->errors();
         }
      } 
      catch (\Throwable $th) {
         $result .= $th->getMessage();
      }
      return $result;
   }   
}