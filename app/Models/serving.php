<?php

namespace App\Models;

use App\Models\menu;
use App\Models\baseModel;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class serving extends baseModel
{
   protected $table = "serving";
   protected $primaryKey = "donationDate";
   protected $keyType = "date";

   protected $fillable = [
      "donationDate",
      "patientID",
      "numPackages",
      "sugar",
      "rice",
      "pork",
      "beef",
      "chicken",
      "fish",
      "egg",
      "cannedFish",
      "oil",
      "veggies",
      "hygiene",
      "received",
      "officer",
      "createdOn",
   ];

   protected $rules = array(
      "donationDate" => "required|date",
      "patientID" => "required|string|max:11",
      "sugar" => "integer",
      "rice" => "integer",
      "pork" => "integer",
      "beef" => "integer",
      "chicken" => "integer",
      "fish" => "integer",
      "egg" => "integer",
      "cannedFish" => "integer",
      "oil" => "integer",
      "veggies" => "string|max:45|nullable",
      "hygiene" => "integer",
      "officer" => "required|string|max:50"
   );


   /**
    * Returns an array with the food for that day + additional food for each patient.
    * The additional food is added by Social Work staff.
    * @param string $dayName
    * @return array
    */
   public function getFoodSupportList() {
      // Initialize variables
      $meatQty = 0;
      $veggie1 = 0;
      $veggiesQty1 = 0;
      $veggie2 = 0;
      $veggiesQty2 = 0;
      $riceKg = 0;
      $eggs = 0;
      $oil = 0;
      $sugar = 0;

      $daysKhmer = array('ច័ន្ទ', 'អង្គារ', 'ពុធ', 'ព្រហស្បតិ៍', 'សុក្រ', 'សៅរ៍', 'អាទិត្យ');
      
      // Get the day of the week (1-7)
      $dayOfWeek = date('N')-1;

      // Get menu of the current day.
      $today = $daysKhmer[$dayOfWeek];
      $menu = new menu();
      $menuOfTheDay = $menu->getMenu($today);

      // Determine the type of meat and quantity from weekly menu.
      $meatType = $menuOfTheDay[0]->meat;
      if (!is_null($meatType)) {
         $cell = explode(" ", $meatType);
         $meatType = $cell[0];
         $meatQty = $cell[1];
      }

      // Determine the vegetables and quantities from weekly menu.
      $veggies = $menuOfTheDay[0]->veggies;
      if (!is_null($veggies)) {
         $cell = explode(" ", $veggies);
         $veggie1 = $cell[0];
         $veggiesQty1 = $cell[1];
         $veggie2 = '';
         $veggiesQty2 = 0;
         if (count($cell) == 4) {
            $veggie2 = $cell[1];
            $veggiesQty2 = is_null($cell[3]) ? 0 : $cell[3];
         }
      }

      // Determine quantity of rice from weekly menu.
      $riceKg = $menuOfTheDay[0]->rice;
      if (!is_null($riceKg)) {
         $riceKg = explode(" ", $riceKg);
         $riceKg = $riceKg[0];
      }

      // Determine number of eggs from weekly menu.
      if (!is_null($eggs)) {
         $eggs = $menuOfTheDay[0]->eggs;
      }

      // Determine quantity of oil from weekly menu.
      $oil = $menuOfTheDay[0]->oil;
      if (!is_null($oil)) {
         $oil = explode(" ", $oil);
         $oil = $oil[0];
      }

      // Determine quantity of sugar from weekly menu.
      $sugar = $menuOfTheDay[0]->sugar;
      if (!is_null($sugar)) {
         $sugar = explode(" ", $sugar);
         $sugar = $sugar[0];
      }

      // Collect all patients still active in food support.
      $query = "SELECT p.patientID,
                      DATE_FORMAT(s.donationDate, '%d-%m-%Y') as donationDate,
                      s.numPackages,
                      p.patientNameEn, 
                      p.patientNameKh,
                      p.dob,
                      p.gender,
                      s.sugar,
                      s.rice,
                      s.pork,
                      s.beef,
                      s.chicken,
                      s.fish,
                      s.egg,
                      s.cannedFish,
                      s.oil,
                      s.veggies,
                      s.hygiene 
                FROM patient p 
                  JOIN supplement s ON (s.patientID = p.patientID) 
                WHERE p.activeFoodSupport <> 'N' 
                ORDER BY s.donationDate";
      $patients = DB::select($query);
      
      // Multiply the quantities of food by the number of packages
      // of each patient.
      for ($i=0; $i < count($patients); $i++) {
         $factor = $patients[$i]->numPackages;
         switch ($meatType) {
            case 'សាច់គោ':   // beef
               $patients[$i]->beef += $factor * $meatQty;
               break;
            case 'ត្រី':       // fish
               $patients[$i]->fish += $factor * $meatQty;
               break;
            case 'សាច់ជ្រូក':  // Pork
               $patients[$i]->beef += $factor * $meatQty;
               break;
            case 'ត្រីកំប៉ុង':   // Canned fish
               $patients[$i]->beef += $factor * $meatQty;
               break;
            case 'សាច់​មាន់':   // Chicken
               $patients[$i]->beef += $factor * $meatQty;
               break;
         }
         if ($veggie1 !== "") {
            $patients[$i]->veggies = $patients[$i]->veggies . " " . $veggie1 . " " . ($factor * $veggiesQty1) . " ";
            if ($veggie2 > 0) {
               $patients[$i]->veggies = $patients[$i]->veggies . $veggie2 . " " . ($factor * $veggiesQty2);
            }
         }
         else {
            $patients[$i]->veggies = "";
         }
         $patients[$i]->rice += $factor * $riceKg;
         $patients[$i]->egg += $factor * $eggs;
         $patients[$i]->oil += $factor * $oil;
         $patients[$i]->sugar += $factor * $sugar;
         $patients[$i]->hygiene += $factor * $menuOfTheDay[0]->hygiene;
      }
      return $patients;
   }


   /**
    * Returns the number of patients that got food support yesterday.
    * @return int $result
    */
   public function foodSupportPatients() {
      $yesterday = date('Y-m-d', strtotime('-1 days'));
      $result = DB::select("SELECT count(1) AS numPatients FROM serving WHERE donationDate = ?", [$yesterday]);
      return $result[0];
   }


   /**
    * Inserts the serving model in the database.
    * Returns an integer value indicating the number of affected rows.
    * @param array $fields
    * @return int|string
    */
   public function insertRow($fields) {
      $result = false;
      // Set default values to model
      $patientID = $fields['patientID'];
      $fields['donationDate'] = date('Y-m-d');
      $fields['sugar'] = isNull($fields['sugar']) ? 0 : $fields['sugar'];
      $fields['rice'] = isNull($fields['rice']) ? 0 : $fields['rice'];
      $fields['pork'] = isNull($fields['pork']) ? 0 : $fields['pork'];
      $fields['beef'] = isNull($fields['beef']) ? 0 : $fields['beef'];
      $fields['chicken'] = isNull($fields['chicken']) ? 0 : $fields['chicken'];
      $fields['fish'] = isNull($fields['fish']) ? 0 : $fields['fish'];
      $fields['egg'] = isNull($fields['egg']) ? 0 : $fields['egg'];
      $fields['cannedFish'] = isNull($fields['cannedFish']) ? 0 : $fields['cannedFish'];
      $fields['oil'] = isNull($fields['oil']) ? 0 : $fields['oil'];
      $fields['veggies'] = isNull($fields['veggies']) ? ' ' : $fields['veggies'];
      $fields['hygiene'] = isNull($fields['hygiene']) ? 0 : $fields['hygiene'];

      if ($this->validate($fields)) {
         try {
            $result = DB::table($this->table)->insert($fields);
            if ($result) {
               $result = DB::table('patient')->where('patientID', $patientID)->update(['lastSeen' => date('Y-m-d')]);
            }
         } 
         catch (\Throwable $th) {
            $result = $th->getMessage();
         }
      }
      return $result;
   }


   /**
    * Removes a patient from the list of food support.
    * @return int The number of affected rows.
    */
   public function removePatient($patientID) {
      return DB::table('patient')->where('patientID', $patientID)->update(['activeFoodSupport'=>'N']);
   }


   /**
    * Calculate the daily number of patients that received food
    * in the current month.
    * @return int|array
    */
   public function monthlyConsumptionReport() {
      $query = "SELECT DAY(donationDate) AS dayOfMonth, 
                       COUNT(1) AS numPatients 
                FROM serving 
                WHERE MONTH(donationDate) = MONTH(NOW()) 
                  AND YEAR(donationDate) = YEAR(NOW()) 
                GROUP BY DAY(donationDate) 
                ORDER BY DAY(donationDate)";
      $result = DB::select($query);
      return (count($result) == 0) ? 0 : $result;
   }
}