<?php

namespace App\Models;

use App\Models\baseModel;
use Illuminate\Support\Facades\DB;

class activity extends baseModel
{
   protected $table = "activity";
   protected $primaryKey = "activityDate";
   protected $keyType = "date";

   protected $fillable = [
      "activityDate",
      "cooking",
      "gardening",
   ];

   protected $rules = array(
      "activityDate" => "required|date",
      "cooking" => "numeric|nullable",
      "gardening" => "numeric|nullable",
   );


   /**
    * Retrieves all the activities attended in the current month.
    * @return array
    */
   public function getAllActivities() {
      $currentMonth = date('m');
      return DB::select("SELECT activityDate, cooking, gardening FROM activity WHERE Month(activityDate) = ?", [$currentMonth]);
   }


   /**
    * Stores the daily patient's parents group activities.
    * Returns the list of activities not saved, if any.
    * @param string $actType
    * @param int $attendances
    * @return string 
    */
   public function saveActivities($cooking, $gardening) {
      $today = date('Y-m-d');
      $result = DB::statement("INSERT INTO activity (activityDate, cooking, gardening) VALUES (?,?,?)", [$today, $cooking, $gardening]);
      return $result;
   }
}