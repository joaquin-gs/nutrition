<?php

namespace App\Models;

use App\Models\baseModel;
use Illuminate\Support\Facades\DB;

class menu extends baseModel {
   protected $table = "menu";
   protected $primaryKey = "donationDate";
   protected $keyType = "date";

   protected $fillable = [
      "dayName",
      "meat",
      "veggies",
      "rice",
      "eggs",
      "oil",
      "sugar",
      "hygiene",
   ];

   protected $rules = array(
      "dayName" => "required|string|max:10",
      "meat" => "string|max:20|nullable",
      "veggies" => "string|max:45|nullable",
      "rice" => "string|max:10|nullable",
      "eggs" => "string|max:10|nullable",
      "oil" => "string|max:10|nullable",
      "sugar" => "string|max:10|nullable",
      "hygiene" => "string|max:45|nullable",
   );

   public $meatTypes = array('សាច់គោ', 'ត្រី', 'សាច់ជ្រូក', 'ត្រីកំប៉ុង', 'សាច់​មាន់');
   public $vegetables = array('សណ្តែក', 'ត្រកួន', 'ស្ពៃក្រញ៉ាញ់', 'ល្ពៅ', 'ខ្ទឹមបារាំង', 'ត្រសក់', 'ប៉េងប៉ោះ');

   /**
    * Retrieves the menu details of a specific day.
    * @param string $weekYear
    * @param string $day
    * @return array $menu
    */
   public function getMenu($dayName) {
      if ($dayName === "" || $dayName == null) {
         // Get the weekly menu.
         $menu = DB::table($this->table)->get()->toArray();
      }
      else {
         // Get the menu of the given day.
         $menu = DB::table($this->table)->where('dayName', $dayName)->get()->toArray();
      }
      return $menu;
   }


   /**
    * Stores the weekly menu in the database.
    * @param array $fields 
    * @returns boolean
    */
   public function store($fields) {
      $result = false;
      // Delete all rows from table menu.
      if ($fields['dayName'] == 'ច័ន្ទ') {
         DB::table($this->table)->delete();
      }
      if ($this->validate($fields)) {
         $result = DB::table($this->table)->insert($fields);
      }
      else {
         $result = $this->errors();
      }
      return $result;
   }

}