USE nutrition;

CREATE TABLE `activity` (
  `activityDate` date NOT NULL,
  `activitiesDetail` varchar(50) NOT NULL,
  `attendances` tinyint(3) unsigned NOT NULL DEFAULT '0',
  CONSTRAINT PK_activity PRIMARY KEY (`activityDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `menu` (
  `weekYear` char(6) NOT NULL,
  `dayName` varchar(10) NOT NULL,
  `meat` varchar(20) DEFAULT NULL,
  `veggies` varchar(45) DEFAULT NULL,
  `rice` varchar(10) DEFAULT NULL,
  `eggs` varchar(10) DEFAULT NULL,
  `oil` varchar(10) DEFAULT NULL,
  `sugar` varchar(10) DEFAULT NULL,
  `hygiene` varchar(45) DEFAULT NULL,
  CONSTRAINT PK_menu PRIMARY KEY (`weekYear`,`dayName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `patient` (
  `patientID` varchar(11) NOT NULL,
  `referredBy` varchar(50) DEFAULT NULL,
  `initialWeight` float DEFAULT NULL,
  `diagnosis` varchar(10) DEFAULT NULL,
  `motherAge` tinyint(4) DEFAULT NULL,
  `numberOfChildren` tinyint(4) DEFAULT NULL,
  `remarks` varchar(400) DEFAULT NULL,
  `hasHIV` char(1) DEFAULT NULL,
  `nutritionCase` tinyint(4) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` char(5) DEFAULT NULL,
  `activeFoodSupport` char(1) NOT NULL DEFAULT 'Y',
  `lastSeen` date DEFAULT NULL,
  CONSTRAINT PK_patient PRIMARY KEY (`patientID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `serving` (
  `weekYear` char(6) NOT NULL,
  `dayName` varchar(10) NOT NULL,
  `patientID` varchar(11) NOT NULL,
  `received` char(1) NOT NULL DEFAULT 'N',
  `officer` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  CONSTRAINT PK_serving PRIMARY KEY (`weekYear`,`dayName`,`patientID`),
  CONSTRAINT `FK_serving_menu` FOREIGN KEY (`weekYear`, `dayName`) REFERENCES `menu` (`weekYear`, `dayName`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_serving_patient` FOREIGN KEY (`patientID`) REFERENCES `patient` (`patientID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `supplement` (
  `weekYear` char(6) NOT NULL,
  `dayName` varchar(10) NOT NULL,
  `patientID` varchar(11) NOT NULL,
  `expiry` date DEFAULT NULL,
  `sugar` varchar(8) DEFAULT NULL,
  `rice` varchar(5) DEFAULT NULL,
  `pork` varchar(8) DEFAULT NULL,
  `beef` varchar(8) DEFAULT NULL,
  `chicken` varchar(8) DEFAULT NULL,
  `fish` varchar(8) DEFAULT NULL,
  `egg` char(2) DEFAULT NULL,
  `cannedFish` varchar(5) DEFAULT NULL,
  `oil` varchar(8) DEFAULT NULL,
  `veggies` varchar(45) DEFAULT NULL,
  `hygiene` varchar(45) DEFAULT NULL,
  CONSTRAINT PK_supplement PRIMARY KEY (`weekYear`,`dayName`,`patientID`),
  CONSTRAINT `FK_supplement_serving` FOREIGN KEY (`weekYear`, `dayName`, `patientID`) REFERENCES `serving` (`weekYear`, `dayName`, `patientID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `treatment_catalog` (
  `treatmentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `treatmentDetail` varchar(50) NOT NULL,
  `deleted` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`treatmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `consultation` (
  `patientID` varchar(11) NOT NULL,
  `visitDate` datetime NOT NULL,
  `fromUnit` int(11) DEFAULT NULL,
  `height` tinyint(4) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `remarks` varchar(400) DEFAULT NULL,
  `treatmentId` tinyint(4) NOT NULL,
  `dischargedOn` datetime DEFAULT NULL,
  `dischargedBy` char(5) DEFAULT NULL,
  PRIMARY KEY (`patientID`,`visitDate`),
  CONSTRAINT `FK_patient_consultation` FOREIGN KEY (`patientID`) REFERENCES `patient` (`patientID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `applied_treatment` (
  `treatmentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patientID` varchar(11) NOT NULL,
  `visitDate` datetime NOT NULL,
  `treatmentType` varchar(50) DEFAULT NULL,
  `subtype1` varchar(40) DEFAULT NULL,
  `subtype2` varchar(40) DEFAULT NULL,
  `quantity` tinyint(4) DEFAULT NULL,
  `frequency` tinyint(4) DEFAULT NULL,
  `duration` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`treatmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
