@extends('adminlte::page')

@section('title', 'Daily food support')

@push('css')
<style>
   @font-face {
      font-family: "Siemreap";
      src: url('{{ asset("fonts/Siemreap.ttf") }}');
   }

   table thead th {
      background-color: #4197c7;
   }

   .delete {
      text-align: center;
   }

   #saveBtn {
      margin-left: 20px;
   }

   .received,
   td:last-child {
      text-align: center;
      cursor: pointer;
   }

   .table td,
   .table th {
      padding: 5px;
   }

   .table tbody tr { height: 24px; }
</style>
@endpush

@section('content_header')
<div class="row justify-content-center">
   <h3>Daily food support</h3>
</div>
@stop

@section('content')
<h5 id="menuOfTheDay"></h5>
<div class="row">
   <div id="menu" class="table-responsive col">
      <table class="table table-bordered table-hover">
         <thead>
            <tr>
               <th title="Meat">សាច់</th>
               <th title="Vegetables">បន្លែ</th>
               <th title="Rice">អង្ករ</th>
               <th title="Egg hen">ពងមាន់</th>
               <th title="Olive oil">ប្រេងសណ្ដែក</th>
               <th title="Sugar">ស្ករស</th>
               <th title="Hygiene pack">កញ្ចប់អនាម័យខ្លួនប្រាណ</th>
            </tr>
         </thead>
         <tbody>
         </tbody>
      </table>
   </div>
</div>
<br>

<div class="row">
   <h5>List of patients and additional food</h5>
</div>
<div id="list" class="table-responsive">
   <table class="table table-bordered table-hover">
      <thead>
         <tr>
            <th>N°</th>
            <th>Added On</th>
            <th>Patient ID</th>
            <th title="Name">ឈ្មោះ</th>
            <th title="DoB">អាយុ</th>
            <th title="Gender">ភេទ</th>
            <th title="Sugar">ស្ករ</th>
            <th title="Rice">អង្ករ</th>
            <th title="Pork">សាច់ជ្រូក</th>
            <th title="Beef">សាច់គោ</th>
            <th title="Chicken">សាច់មាន់</th>
            <th title="Fish">ត្រី</th>
            <th title="Egg">ពងមាន់</th>
            <th title="Canned fish">ត្រីខ</th>
            <th title="Oil">ប្រេងឆា</th>
            <th title="Vegetables">បន្លែ</th>
            <th title="Hygiene pack">កញ្ចប់អនាម័យខ្លួនប្រាណ</th>
            <th title="Received">បានទទួល</th>
            <th></th>
         </tr>
      </thead>
      <tbody>
      </tbody>
   </table>
</div>
<div class="row justify-content-center">
   <div class="col text-center">
      <button type="button" id="saveBtn" class="btn btn-outline-primary col-2">Save</button>
      <span class="col-1">&nbsp;</span>
      <button type="button" id="cancelBtn" class="btn btn-outline-secondary col-2">Close</button>
   </div>
</div>
<br>
@stop

@push('js')
<script type="text/javascript" src="{{ asset('/js/utils.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/serving.js') }}"></script>
@endpush