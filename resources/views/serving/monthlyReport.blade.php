@extends('adminlte::page')

@section('title', 'Servings monthly report')

@push('css')
<style>
   @font-face {
      font-family: "Siemreap";
      src: url('{{ asset("fonts/Siemreap.ttf") }}');
   }

   table thead th { background-color: #4197c7; }

   .table td,
   .table th { padding: 5px; }

   .table tbody tr { height: 24px; }
</style>
@endpush

@section('content_header')
<div class="row justify-content-center">
   <h3>Servings monthly report</h3>
</div>
@stop

@section('content')
<br>
<div class="row">
   <h5>Food support total patients ({{ date('F Y') }})</h5>
</div>

<div id="list" class="table-responsive">
   <table class="table table-bordered table-hover col-sm-2">
      <thead>
         <tr>
            <th>Day</th>
            <th>Total</th>
         </tr>
      </thead>
      <tbody>
      @foreach ($servings as $serving)
         <tr>
            <td>{{ $serving->dayOfMonth }}</td>
            <td>{{ $serving->numPatients }}</td>
         </tr>
      @endforeach
      </tbody>
   </table>
   <label class="col-1">Total:</label><span id="totPatients"></span>
</div>

<div class="row justify-content-center">
   <div class="col text-center">
      <button type="button" id="cancelBtn" class="btn btn-outline-secondary col-2">Close</button>
   </div>
</div>
<br>
@stop

@push('js')
<script type="text/javascript" src="{{ asset('js/utils.js') }}"></script>
<script>
   $(() => {
      var numP = 0;
      var totalM = 0;
      $('table tbody tr').each((index, elem)=>{
         numP = $(elem).children('td').eq(1).text() * 1;
         totalM = totalM + numP;
      });
      $('#totPatients').text(totalM);

      $('#cancelBtn').on('click', function() {
         window.location = '/';
      });
      
   })
</script>
@endpush