@extends('adminlte::page')

@push('css')
<style>
   #card {
      margin: 0;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      z-index: 100000;
   }
   #backdrop {
      background-color: transparent;
      position: fixed;
      top: 0;
      left: 0;
      z-index: 99999;
   }
</style>
@endpush

@section('content')
<div id="backdrop"></div>
<div class="container">
   <div id="card" class="col-4 card card-warning">
      <div class="card-header">
         <h3 class="card-title">Approval required</h3>
      </div>
      <div class="card-body text-center"><h2>Your account is waiting for approval.</h2></div>
      <div class="row justify-content-center">
         <button id="closeBtn" class="btn btn-outline-primary col-2 mb-5">Close</button>
      </div>
   </div>
</div>
@endsection

@push('js')
<script>
   $('.nav.nav-pills.nav-sidebar > li a').each(function (index, elem) {
      $(elem).attr('href', '');
   });

   $('#backdrop').css('width', window.screen.width);
   $('#backdrop').css('height', window.screen.height);

   $('#closeBtn').on('click', function() {
      document.getElementById('logout-form').submit();
   })
</script>
@endpush