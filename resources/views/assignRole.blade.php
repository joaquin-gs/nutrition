@extends('adminlte::page')

@push('css')
<style>
   .userCell { padding-top: 18px !important; }
</style>
@endpush

@section('content')
<div class="container">
   <br />
   <div class="row">
      <div class="col text-center">
         <h4>User role assignment</h4>
      </div>
   </div>
   <br />

   <form id="inputForm" method="POST" action="">
      @csrf
      <div class="row justify-content-center">
         <div class="table-responsive col-6">
            <table id="tRoles" class="table table-bordered">
               <thead>
                  <tr>
                     <td class="col-3"><strong>User</strong></td>
                     <td class="col-3"><strong>Role</strong></td>
                  </tr>
               </thead>
               <tbody>
                  @foreach ($users as $user)
                  <tr>
                     <td class="userId d-none">{{ $user->id }}</td>
                     <td class="userCell">{{ $user->name }}</td>
                     <td class="roleId d-none">{{ $user->role }}</td>
                     <td>
                        <select class="role custom-select">
                           <option value="0">Please select</option>
                           @foreach ($roles as $rol)
                           @if ($user->role == $rol->roleID)
                           <option value="{{ $rol->roleID }}" selected="selected">{{ $rol->roleName }}</option>
                           @else
                           <option value="{{ $rol->roleID }}">{{ $rol->roleName }}</option>
                           @endif
                           @endforeach
                        </select>
                     </td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>

      <br />
      <div id="buttons" class="form-group row justify-content-center">
         <input type="button" id="saveRoles" value="Save" class="btn btn-outline-primary col-2 mr-5" />
         <input type="button" id="cancelBtn" value="Cancel" class="btn btn-outline-secondary col-2" />
      </div>
   </form>
</div>
@endsection

@push('js')
<script>
   $(() => {
      $('#saveRoles').on('click', function() {
         // Build a JSON string with table key fields
         var userId = '';
         var roleId = '';
         var roles = '[';
         $('#tRoles tbody tr').each(function(index) {
            userId = $(this).find('td.userId').text();
            roleId = $(this).find('.role option:selected').val();
            if (roleId == '') {
               roleId = 0;
            }
            roles += `{"userID": "${userId}", "roleID": "${roleId}"},`;
         });
         roles = roles.substr(0, roles.length - 1) + ']';
         console.log(roles);
         roles = JSON.parse(roles);

         jQuery.ajax({
            type: 'post',
            url: '/saveRoles',
            data: {
               _token: $('meta[name="csrf-token"]').attr('content'),
               roles: roles,
            },
            success: function(response) {
               alert('The roles have been assigned.');
               window.location = '/';
            },
            error: function(xhr, status, error) {
               console.log(xhr.status + " / " + xhr.responseJSON.message);
            }
         });
      });


      $('#cancelBtn').on('click', function() {
         window.location = '/';
      });
   });
</script>
@endpush
