@extends('adminlte::page')

@section('title', 'Daily food support')

@push('css')
<style>
   @font-face {
      font-family: "Siemreap";
      src: url('{{ asset("fonts/Siemreap.ttf") }}');
   }

   table thead th {
      background-color: #4197c7;
   }

   .form-row {
      margin-bottom: 5px !important;
   }
</style>
@endpush

@section('content_header')
<div class="row justify-content-center">
   <h3>Daily food support</h3>
</div>
@stop

@section('content')
<div class="wrapper">

   <div class="form-row">
      <label for="patientID" class="col-2 col-form-label">Patient ID</label>
      <input type="text" name="patientID" id="patientID" value="" class="col-2 form-control" maxlength="11">
      <span class="col-sm-1"></span>
      <label class="col-3 col-form-label" id="patientName"></label>
   </div>
   <div class="form-row">
      <label for="packages" class="col-2 col-form-label">Number of packages</label>
      <input type="text" name="packages" id="packages" value="1" class="col-1 form-control numeric" maxlength="1">
   </div>
   <br>

   <div class="row">
      <h5>Additional requested food</h5>
   </div>
   <div class="form-row">
      <label for="sugar" class="col-2 col-form-label" title="Sugar">ស្ករស</label>
      <input type="text" name="sugar" id="sugar" value="" maxlength="4" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">ខាំ</span>
   </div>
   <div class="form-row">
      <label for="rice" class="col-2 col-form-label" title="Rice">ស្ករស</label>
      <input type="text" name="rice" id="rice" value="" maxlength="1" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">គ.ក</span>
   </div>
   <div class="form-row">
      <label for="pork" class="col-2 col-form-label" title="Pork">សាច់ជ្រូក</label>
      <input type="text" name="pork" id="pork" value="" maxlength="4" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">ខាំ</span>
   </div>
   <div class="form-row">
      <label for="beef" class="col-2 col-form-label" title="Beef">សាច់គោ</label>
      <input type="text" name="beef" id="beef" value="" maxlength="4" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">ខាំ</span>
   </div>
   <div class="form-row">
      <label for="chicken" class="col-2 col-form-label" title="Chicken">សាច់​មាន់</label>
      <input type="text" name="chicken" id="chicken" value="" maxlength="4" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">ខាំ</span>
   </div>
   <div class="form-row">
      <label for="fish" class="col-2 col-form-label" title="Fish">ត្រី</label>
      <input type="text" name="fish" id="fish" value="" maxlength="4" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">ខាំ</span>
   </div>
   <div class="form-row">
      <label for="eggs" class="col-2 col-form-label" title="Eggs">ពងមាន់</label>
      <input type="text" name="eggs" id="eggs" value="" maxlength="1" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">គ្រាប់</span>
   </div>
   <div class="form-row">
      <label for="cannedFish" class="col-2 col-form-label" title="Canned Fish">ត្រីកំប៉ុង</label>
      <input type="text" name="cannedFish" id="cannedFish" value="" maxlength="1" class="col-1 form-control numeric">&nbsp;<span class="col-form-label font-weight-bold">ត្រីខ</span>
   </div>
   <div class="form-row">
      <label for="oil" class="col-2 col-form-label" title="Oil">ប្រេងសណ្ដែក</label>
      <input type="text" name="oil" id="oil" value="" maxlength="4" class="col-1 form-control">&nbsp;<span class="col-form-label font-weight-bold">ខាំ</span>
   </div>
   <div class="form-row">
      <label for="veggies" class="col-2 col-form-label" title="Vegetables">បន្លែ</label>
      <input type="text" name="veggies" id="veggies" value="" maxlength="45" class="col-3 form-control khmer">&nbsp;<span class="col-form-label font-weight-bold">ខាំ</span>
   </div>
   <div class="form-row">
      <label for="hygiene" class="col-2 col-form-label" title="Hygiene pack">កញ្ចប់អនាម័យខ្លួនប្រាណ</label>
      <input type="text" name="hygiene" id="hygiene" value="" maxlength="1" class="col-1 form-control numeric">
   </div>
   <br>

   <div class="row justify-content-center">
      <button id="saveBtn" class="btn btn-primary col-sm-1" type="button" disabled>Submit</button>
      <span class="col-sm-1"></span>
      <button id="cancelBtn" class="btn btn-secondary col-sm-1" type="button">Close</button>
   </div>

</div>

<div id="myModal" class="modal" tabindex="-1" data-backdrop="static" role="dialog">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">

         <div class="modal-header">
            <h5 class="modal-title">Patient's name</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>

         <div id="modal-body" class="modal-body">
            <div class="form-row">
               <input type="text" id="khmerName" class="khmer col-5 mr-2 form-control" value="" maxlength="45" placeholder="ឈ្មោះ">
               <input type="text" id="englishName" class="col-5 form-control mr-2" value="" maxlength="45" placeholder="English name">
            </div>
            <br>

            <div class="form-row">
               <label class="col-2 col-form-label">Gender</label>
               <select id="sex" class="form-control col-2">
                  <option value="" selected></option>
                  <option value="M">Male</option>
                  <option value="F">Female</option>
               </select>
            </div>
            <br>

            <h5>Date of birth</h5>
            <div class="form-row">
               <label class="col-1 col-form-label">Year</label>
               <input type="text" id="year" class="numeric form-control col-2" value="" maxlength="4">
               <label class="col-form-label">Month</label>
               <input type="text" id="month" class="numeric form-control col-1" value="" maxlength="2">
               <label class="col-form-label">Day</label>
               <input type="text" id="day" class="numeric form-control col-1" value="" maxlength="2">
            </div>
         </div>

         <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="acceptData">Accept data</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Cancel</button>
         </div>

      </div>
   </div>
</div>
@stop

@push('js')
<script type="text/javascript" src="{{ asset('/js/utils.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/supplement.js') }}"></script>
@endpush