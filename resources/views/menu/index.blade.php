@extends('adminlte::page')

@section('title', 'Nutrition: Weekly menu')

@push('css')
<style>
   @font-face {
      font-family: "Siemreap";
      src: url('{{ asset("fonts/Siemreap.ttf") }}');
   }

   .modal-header {
      background-color: #3f94c5;
      font-family: "Siemreap";
      font-weight: bold;
   }
</style>
<link rel="stylesheet" href="{{ asset('/css/menu.css') }}">
@endpush

@section('content_header')
<div class="row">
   <h3 id="weekNumber">Daily menu</h3>
</div>
@stop

@section('content')
<div class="containerx">
   <form id="formMenu" action="" method="post" autocomplete="on">
      <div class="row justify-content-center">
         <div class="table-responsive col">
            <table class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th class="colHeader" title="Days of the week">ថ្ងៃ​នៃ​ស​ប្តា​ហ៍</th>
                     <th class="col1" title="Meat">សាច់</th>
                     <th class="col2" title="Vegetables">បន្លែ</th>
                     <th class="col3" title="Rice">អង្ករ</th>
                     <th class="col4" title="Egg hen">ពងមាន់</th>
                     <th class="col5" title="Olive oil">ប្រេងសណ្ដែក</th>
                     <th class="col6" title="Sugar">ស្ករស</th>
                     <th class="col7" title="Hygiene pack">កញ្ចប់អនាម័យខ្លួនប្រាណ</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </form>

   <div class="row justify-content-center">
      <div class="col text-center">
         <button type="button" id="saveBtn" class="btn btn-outline-primary">Submit</button>
         <label class="spacer">&nbsp;</label>
         <button type="button" id="cancelBtn" class="btn btn-outline-secondary">Close</button>
      </div>
   </div>
</div>

<div id="mymodal" class="modal" tabindex="-1" data-backdrop="static" role="dialog">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><label aria-hidden="true">&times;</label></button>
         </div>
         <div id="modal-body" class="modal-body">
            <div class="row">
               <label class="col-4" title="Food">អាហារ</label>
               <label class="col" title="Quantity">បរិមាណ</label>
            </div>
            <div id="body-content"></div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="acceptData">Accept data</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Cancel</button>
         </div>
      </div>
   </div>
</div>

<div id="meat" class="row hidden">
   <div class="col">
      <select id="meatList" class="custom-select">
         <option value="Beef" title="Beef">សាច់គោ</option>
         <option value="Fish" title="Fish">ត្រី</option>
         <option value="Pork" title="Pork">សាច់ជ្រូក</option>
         <option value="Canned fish" title="Canned fish">ត្រីកំប៉ុង</option>
         <option value="Chicken" title="Chicken">សាច់​មាន់</option>
      </select>
   </div>

   <div class="col">
      <input type="text" class="qty form-control numeric" value="" autofocus>
   </div>

   <div class="col">
      <label class="measurement col-form-label">ខាំ</label>
   </div>
</div>

<div id="veggies" class="row hidden">
   <div class="row">
      <div class="col">
         <select id="veggiesList1" class="custom-select">
            <option value="Beans" title="Beans">សណ្តែក</option>
            <option value="Morning glory" title="Morning glory">ត្រកួន</option>
            <option value="Cabbage" title="Cabbage">ស្ពៃក្រញ៉ាញ់</option>
            <option value="Pumpkin" title="Pumpkin">ល្ពៅ</option>
            <option value="Onion" title="Onion">ខ្ទឹមបារាំង</option>
            <option value="Cucumber" title="Cucumber">ត្រសក់</option>
            <option value="Tomato" title="Tomato">ប៉េងប៉ោះ</option>
         </select>
      </div>

      <div class="col">
         <input type="text" class="qty1 form-control numeric" value="" autofocus>
      </div>

      <div class="col">
         <label class="measurement1 col-form-label">ខាំ</label>
      </div>
   </div>

   <div class="row">
      <div class="col">
         <select id="veggiesList2" class="custom-select">
            <option value="Beans" title="Beans">សណ្តែក</option>
            <option value="Morning glory" title="Morning glory">ត្រកួន</option>
            <option value="Cabbage" title="Cabbage">ស្ពៃក្រញ៉ាញ់</option>
            <option value="Pumpkin" title="Pumpkin">ល្ពៅ</option>
            <option value="Onion" title="Onion">ខ្ទឹមបារាំង</option>
            <option value="Cucumber" title="Cucumber">ត្រសក់</option>
            <option value="Tomato" title="Tomato">ប៉េងប៉ោះ</option>
         </select>
      </div>

      <div class="col">
         <input type="text" class="qty2 form-control" value="">
      </div>

      <div class="col">
         <label class="measurement2 col-form-label">ខាំ</label>
      </div>
   </div>
</div>

<div id="rice" class="row hidden">
   <label class="col col-form-label">Rice </label>
   <input type="text" class="col qty form-control numeric" maxlength="2" value="" autofocus>
   <label class="col col-form-label">គ.ក</label>
</div>

<div id="oil" class="row hidden">
   <label class="col col-form-label">Oil </label>
   <input type="text" class="col qty form-control numeric" maxlength="4" value="" autofocus>
   <label class="col col-form-label">ខាំ</label>
</div>

<div id="sugar" class="row hidden">
   <label class="col col-form-label">Sugar </label>
   <input type="text" class="col qty form-control numeric" maxlength="4" value="" autofocus>
   <label class="col col-form-label">ខាំ</label>
</div>
@stop

@push('js')
<script type="text/javascript" src="{{asset('/js/utils.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/menu.js')}}"></script>
@endpush