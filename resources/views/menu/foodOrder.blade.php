@extends('adminlte::page')

@section('title', 'Nutrition: Food Order')

@push('css')
<style>
   @font-face {
      font-family: "Siemreap";
      src: url('{{ asset("fonts/Siemreap.ttf") }}');
   }

   .food { width: 10%; }

   .foodType { width: 40%; }

   .qty { width: 10%; }

   .measurement { width: 10%; }
</style>
<link rel="stylesheet" href="{{ asset('/css/menu.css') }}">
@endpush

@section('content_header')
<div class="row justify-content-center">
   <h3>Food Order</h3>
</div>
@stop

@section('content')
<div class="row justify-content-center">
   <h5>List of food for tomorrow</h5>
</div>
<div class="container">
   <div class="row justify-content-center">
      <div id="list" class="table-responsive col-8">
         <table class="table table-bordered table-hover">
            <thead>
               <tr>
                  <th class="food">Menu item</th>
                  <th class="foodType">Food detail</th>
                  <th class="qty">Quantity</th>
                  <th class="measurement">Measurement</th>
               </tr>
            </thead>
            <tbody>
               <tr id="meat">
                  <th class="food" title="Meat">សាច់</th>
                  <td class="foodType"></td>
                  <td class="qty"></td>
                  <td class="measurement"></td>
               </tr>
               <tr id="veggies">
                  <th class="food" title="Vegetables">បន្លែ</th>
                  <td class="foodType"></td>
                  <td class="qty"></td>
                  <td class="measurement"></td>
               </tr>
               <tr id="rice">
                  <th class="food" title="Rice">អង្ករ</th>
                  <td class="foodType"></td>
                  <td class="qty"></td>
                  <td class="measurement"></td>
               </tr>
               <tr id="egg">
                  <th class="food" title="Egg hen">ពងមាន់</th>
                  <td class="foodType"></td>
                  <td class="qty"></td>
                  <td class="measurement"></td>
               </tr>
               <tr id="oil">
                  <th class="food" title="Olive oil">ប្រេងសណ្ដែក</th>
                  <td class="foodType"></td>
                  <td class="qty"></td>
                  <td class="measurement"></td>
               </tr>
               <tr id="sugar">
                  <th class="food" title="Sugar">ស្ករស</th>
                  <td class="foodType"></td>
                  <td class="qty"></td>
                  <td class="measurement"></td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>
<div class="row justify-content-center">
   <button type="button" id="cancelBtn" class="btn btn-outline-secondary">Close</button>
</div>
@stop

@push('js')
<script src="{{ asset('js/app.js') }}" defer></script>
<script type="text/javascript" src="{{asset('/js/utils.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/menu.js')}}"></script>
@endpush