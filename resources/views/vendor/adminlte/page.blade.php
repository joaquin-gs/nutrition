@extends('adminlte::master')

@inject('layoutHelper', 'JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@section('adminlte_css')
@stack('css')
<style>
   #exit {
      border-bottom: none;
      position: fixed;
      bottom: 0;
   }

   #exitBtn {
      width: 50px;
   }

   #exitBtn:hover {
      background-color: #494e54;
      border-radius: .25rem;
   }

   #exitBtn img {
      margin: 5px 5px 5px 10px;
   }

   #exitBtn span {
      display: none;
   }

   .main-sidebar:hover .sidebar #exit #exitBtn {
      width: 235px;
   }

   .main-sidebar:hover .sidebar #exit #exitBtn span {
      display: inline-block;
   }
</style>
@yield('css')
@stop

@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('body')
<div class="wrapper">

   {{-- Top Navbar 
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.navbar.navbar-layout-topnav')
        @else
            @include('adminlte::partials.navbar.navbar')
        @endif 
   --}}

   {{-- Left Main Sidebar --}}
   @if(!$layoutHelper->isLayoutTopnavEnabled())
      @include('adminlte::partials.sidebar.left-sidebar')
   @endif

   {{-- Content Wrapper --}}
   @empty($iFrameEnabled)
      @include('adminlte::partials.cwrapper.cwrapper-default')
   @else
      @include('adminlte::partials.cwrapper.cwrapper-iframe')
   @endempty

   {{-- Footer --}}
   @hasSection('footer')
      @include('adminlte::partials.footer.footer')
   @endif

   {{-- Right Control Sidebar --}}
   @if(config('adminlte.right_sidebar'))
      @include('adminlte::partials.sidebar.right-sidebar')
   @endif

</div>
@stop

@section('adminlte_js')
@stack('js')
<script>
   window.currentUser = "{{ Auth::user()->name }}";
</script>
@yield('js')
@stop