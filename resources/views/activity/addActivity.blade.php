@extends('adminlte::page')

@section("title", "Nutrition: Add today's activities")

@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('js/jqwidgets/styles/jqx.base.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('js/jqwidgets/styles/jqx.darkblue.css')}}">
<style>
   #saveBtn {
      margin-right: 20px;
   }

   #toolbarjqxGrid div h5 {
      margin: 5px 0 0 10px;
   }
</style>
@endpush

@section('content_header')
<div class="row justify-content-center">
   <h3>Add today's activities</h3>
</div>
@stop

@section('content')
<div class="container">
   <div class="form-row justify-content-center">
      <div class="col col-6">
         <div class="row">
            <label class="col" for="">Activities:</label>
         </div>
         <div class="row">
            <div class="col">
               <label for="cooking" class="col-form-label col-2">Cooking</label>
               <input type="text" id="cooking" class="numeric form-control-sm col-sm-1" maxlength="2">
            </div>
         </div>
         <div class="row">
            <div class="col">
               <label for="gardening" class="col-form-label col-2">Gardening</label>
               <input type="text" id="gardening" class="numeric form-control-sm col-sm-1" maxlength="2">
            </div>
         </div>
      </div>
      <br>
   </div>
   <br>
   <div class="row justify-content-center">
      <div class="col-6">
         <button id="saveBtn" type="button" class="form-control btn btn-sm btn-outline-primary col-2" value="Save">Save</button>
         <button type="button" id="cancelBtn" class="btn btn-outline-secondary col-2">Close</button>
      </div>
   </div>
   <br>

   <div class="row justify-content-center">
      <div class="col-sm-10 col-md-10 col-lg-6">
         <div id="jqxGrid"></div>
      </div>
   </div>
   <br>
</div>

@stop

@push('js')
<script type="text/javascript" src="{{ asset('js/jqwidgets/jqx-all.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/activity.js') }}"></script>
@endpush