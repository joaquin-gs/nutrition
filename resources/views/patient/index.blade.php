@extends('adminlte::page')

@section('title', 'Nutrition: Patients')

@push('css')
<style>
   @font-face {
      font-family: "Siemreap";
      src: url('{{ asset("fonts/Siemreap.ttf") }}');
   }

   .btn {
      min-width: 90px;
      margin-right: 20px;
   }

   .grid_buttons { margin: 3px 0px 0px 3px; }

   ul li { list-style: none; }

   #page-container { height: 100%; }

   .page {
      display: none;
      width: 100%;
   }

   .page.active { display: inline-block; }

   .form-row { margin-bottom: 5px !important; }

   #sam label, #cleft label { margin: 0px 10px; }

   .chkBox { transform: scale(1.5); }

   .spacer {
      width: 118px;
      display: inline-block;
   }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('js/jqwidgets/styles/jqx.base.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('js/jqwidgets/styles/jqx.darkblue.css') }}">
@endpush

@section('content_header')
<div class="content-header"></div>
@stop

@section('content')
<ul id="page-container">
   <li id="patientsGrid" class="page active">
      <div class="page-header">
         <div class="row justify-content-center">
            <h3>Patients</h3>
         </div>
      </div>
      <div class="page-content">
         <div class="row justify-content-center">
            <div id="grid"></div>
         </div>
      </div>
   </li>

   <li id="registerPatient" class="page">
      <div class="page-header">
         <div class="row justify-content-center">
            <h3>Register patient</h3>
         </div>
      </div>
      <div class="page-content container">
         <div class="form-horizontal">
            <div class="form-row">
               <label for="patientID" class="col-2 col-form-label">Patient ID</label>
               <div class="col-2">
                  <input type="text" id="patientID" maxlength="11" class="form-control">
               </div>
            </div>

            <div class="form-row">
               <label class="col-2 col-form-label">Patient name</label>
               <div class="col-3">
                  <span id="patientNameKh" class="form-control"></span>
               </div>
            </div>

            <div class="form-row">
               <label class="col-2 col-form-label">Address</label>
               <div class="col-10 form-inline">
                  <div id="province_name_khmer" class="form-control col-2 mr-2"></div>
                  <div id="district_name_khmer" class="form-control col-2 mr-2"></div>
                  <div id="commune_name_khmer" class="form-control col-2 mr-2"></div>
                  <div id="village_name_khmer" class="form-control col-2"></div>
               </div>
            </div>

            <div class="form-row">
               <label class="col-2 col-form-label">Phone number</label>
               <div class="col-3">
                  <div id="phone1" class="form-control"></div>
               </div>
            </div>

            <div class="form-row">
               <label class="col-2 col-form-label">Date of birth</label>
               <div class="col-3">
                  <span id="date_birth" class="form-control"></span>
               </div>
            </div>

            <div class="form-row">
               <label for="referredBy" class="col-2 col-form-label">Referred by</label>
               <div class="col-3">
                  <select id="referredBy" class="form-control">
                     <option value="" selected></option>
                     <option value="1">Ward nurse</option>
                     <option value="2">Inpatient doctor</option>
                     <option value="3">OPD doctor</option>
                     <option value="4">Self referral</option>
                     <option value="5">Nutrition team identified</option>
                     <option value="6">Other</option>
                  </select>
               </div>
            </div>

            <div class="form-row">
               <label for="initialWeight" class="col-2 col-form-label">Initial weight</label>
               <div class="col-1">
                  <input type="text" id="initialWeight" maxlength="10" class="form-control numeric">
               </div>
            </div>

            <div class="form-row">
               <label for="diagnosis" class="col-2 col-form-label">Diagnosis</label>
               <div class="col-3">
                  <select id="diagnosis" class="form-control">
                     <option value="" selected></option>
                  </select>
               </div>
            </div>

            <div class="form-row">
               <label for="motherAge" class="col-2 col-form-label">Mother's age</label>
               <div class="col-1">
                  <input type="text" id="motherAge" maxlength="2" class="form-control numeric">
               </div>
            </div>

            <div class="form-row">
               <label for="numberOfChildren" class="col-2 col-form-label">Number previous children</label>
               <div class="col-1">
                  <input type="text" id="numberOfChildren" maxlength="2" class="form-control numeric">
               </div>
            </div>

            <div class="form-row">
               <label for="delivery" class="col-2 col-form-label">Delivery method</label>
               <div class="col-2">
                  <select id="delivery" class="form-control">
                     <option value="" selected></option>
                     <option value="1">NVD</option>
                     <option value="2">Vacuum-assisted</option>
                     <option value="3">C-Section</option>
                  </select>
               </div>
            </div>

            <div class="form-row">
               <label for="remarks" class="col-2 col-form-label">Remarks</label>
               <div class="col-3">
                  <textarea id="remarks" cols="60" rows="3"></textarea>
               </div>
            </div>

            <div class="form-row">
               <label class="col">Has HIV<span class="spacer"></span><input type="checkbox" id="hasHIV" class="col-1 form-check-input chkBox"></label>
            </div>

            <div class="form-row">
               <label for="nutritionCase" class="col-2 col-form-label">Nutrition case</label>
               <div class="col-3">
                  <select id="nutritionCase" class="form-control">
                     <option value="" selected></option>
                     <option value="1">MAM</option>
                     <option value="2">SAM</option>
                     <option value="3">Cleft/Oral structure abnormality</option>
                     <option value="4">Breastfeeding counseling</option>
                     <option value="5">Feeding counseling</option>
                     <option value="6">Other</option>
                  </select>
               </div>

               <div id="sam" class="col d-none">
                  <input type="checkbox" id="" class="chkBox"><label for="">Marasmus</label><span class="col-1"></span>
                  <input type="checkbox" id="" class="chkBox"><label for="">Kwashiokor</label>
               </div>

               <div id="cleft" class="col d-none">
                  <input type="checkbox" id="" class="chkBox"><label for="">Palette</label>
                  <input type="checkbox" id="" class="chkBox"><label for="">Lip</label>
                  <input type="checkbox" id="" class="chkBox"><label for="">Mouth</label>
                  <input type="checkbox" id="" class="chkBox"><label for="">Other</label>
               </div>
            </div>

            <br>
            <div class="form-row justify-content-center">
               <button id="submitRegisterForm" class="btn btn-outline-primary">Submit</button>
               <span class="col-sm-1"></span>
               <button id="closeRegisterForm" class="btn btn-outline-primary">Cancel</button>
            </div>

         </div>
      </div>
   </li>
</ul>
@stop

@push('js')
<script src="{{ asset('js/jqwidgets/jqx-all.js') }}"></script>
<script src="{{ asset('js/patient.js') }}"></script>
@endpush