/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import {createApp} from 'vue';
import App from './App.vue'
import gridBtns from './components/gridBtns.vue'

// let url = window.location.pathname
// switch (url) {
//    case '/supplement/index':
//       break
//    case '/patient/index':
//       createApp(gridBtns).mount('#grid')
//       break
//    case '/activity/add':
//       break
//    case '/menu/index':
//       break
//    case '/serving/index':
//       break
//    case '/menu/foodOrder':
//       //
// }

//createApp(App).mount('#app')