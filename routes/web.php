<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\activityController;
use App\Http\Controllers\menuController;
use App\Http\Controllers\servingController;
use App\Http\Controllers\supplementController;
use App\Http\Controllers\patientController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('home', [HomeController::class, 'index'])->name('home');
Route::get('/assignRole', [HomeController::class, 'assignRole']);
Route::post('/saveRoles', [HomeController::class, 'saveRoles']);

Route::get('/activity/getAll', [activityController::class, 'getAllActivities']);
Route::get('/activity/add', [activityController::class, 'addActivity']);
Route::post('/activity/saveActivities', [activityController::class, 'saveActivities']);

Route::get('/menu/index', [menuController::class, 'index']);
Route::get('/menu/getMenu', [menuController::class, 'getMenu']);
Route::post('/menu/store', [menuController::class, 'store']);
Route::get('/menu/foodOrder', [menuController::class, 'createFoodOrder']);

Route::get('/serving/index', [servingController::class, 'index']);
Route::get('/serving/getFoodSupportList', [servingController::class, 'getFoodSupportList']);
Route::post('/serving/insert', [servingController::class, 'insert']);
Route::post('/serving/removePatient', [servingController::class, 'removePatient']);
Route::get('/serving/foodSupportPatients', [servingController::class, 'foodSupportPatients']);
Route::get('/serving/report', [servingController::class, 'monthlyConsumptionReport']);

Route::get('/supplement/index', [supplementController::class, 'index']);
Route::get('/supplement/getPatient', [supplementController::class, 'getPatient']);
Route::post('/supplement/store', [supplementController::class, 'store']);
Route::get('/supplement/foodSupportStatus', [supplementController::class, 'foodSupportStatus']);

Route::get('/patient/index', [patientController::class, 'index']);
Route::get('/patient/getAllPatients', [patientController::class, 'getAllPatients']);
Route::get('/patient/register', [patientController::class, 'register']);
